package projekat.poslovna.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.ObracunatiPorezi;
import projekat.poslovna.entity.Porez;
import projekat.poslovna.entity.StavkeFakture;
import projekat.poslovna.session.FakturaFacadeLocal;
import projekat.poslovna.session.GrupaProizvodaFacadeLocal;
import projekat.poslovna.session.ObracunatiPoreziFacadeLocal;
import projekat.poslovna.session.ProizvodFacadeLocal;
import projekat.poslovna.session.StavkeFaktureFacadeLocal;
import projekat.poslovna.transport.StavkeFaktureTransportObject;

@Path("/stavke_fakture")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StavkeFaktureRest {
	@EJB
	StavkeFaktureFacadeLocal bean;
	@EJB 
	ProizvodFacadeLocal proizvodBean;
	@EJB
	FakturaFacadeLocal fakturaBean;
	@EJB
	ObracunatiPoreziFacadeLocal obracunatiPoreziBean;
	@EJB
	GrupaProizvodaFacadeLocal grupaProizvodaBean;
	
	@GET
	public List<StavkeFaktureTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return StavkeFaktureTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public StavkeFaktureTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new StavkeFaktureTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(StavkeFaktureTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkeFakture temp = new StavkeFakture(param.kolicina, param.rabat, param.jedinicnaCena, param.stopaPdvA, param.osnovica, param.iznosPdvA, param.ukupanIznos);
			temp.setIdFakture(fakturaBean.find(param.idFakture));
			temp.setIdProizvoda(proizvodBean.find(param.idProizvoda));
			bean.create(temp);
			
			//azuriranje tabele faktura
			Faktura tempFakt = fakturaBean.find(param.idFakture);
			float ukupanRabat = tempFakt.getUkupanRabat() + temp.getOsnovica()*temp.getRabat()/100;
			float ukupanIznosBezPDV = tempFakt.getUkupanIznosBezPdvA() + temp.getOsnovica();
			float ukupanPDV = tempFakt.getUkupanPdv() + temp.getIznosPdvA();
			float ukupnoZaPlacanje = tempFakt.getUkupnoZaPlacanje() + (temp.getOsnovica() - temp.getOsnovica()*temp.getRabat()/100 + temp.getIznosPdvA());
			tempFakt.setUkupanRabat(ukupanRabat);
			tempFakt.setUkupanIznosBezPdvA(ukupanIznosBezPDV);
			tempFakt.setUkupanPdv(ukupanPDV);
			tempFakt.setUkupnoZaPlacanje(ukupnoZaPlacanje);
			fakturaBean.edit(tempFakt);
			
			//azuriranje tabele obracunati porezi
			Porez tempPorez = (temp.getIdProizvoda()).getIdGrupe().getIdPoreza();
			ObracunatiPorezi tempObracunatPorezZaStavkuFakt = null;
			for (ObracunatiPorezi x : obracunatiPoreziBean.findAll()) {
				if (x.getIdFakture().getIdFakture().intValue() == tempFakt.getIdFakture().intValue() &&
						x.getIdPoreza().getIdPoreza().intValue() == tempPorez.getIdPoreza().intValue()) {
					tempObracunatPorezZaStavkuFakt = x;
					break;
				}
			}
			if (tempObracunatPorezZaStavkuFakt != null) {
				tempObracunatPorezZaStavkuFakt.setIznos(tempObracunatPorezZaStavkuFakt.getIznos() + temp.getIznosPdvA());
				obracunatiPoreziBean.edit(tempObracunatPorezZaStavkuFakt);
			} else {
				tempObracunatPorezZaStavkuFakt = new ObracunatiPorezi();
				tempObracunatPorezZaStavkuFakt.setIdFakture(tempFakt);
				tempObracunatPorezZaStavkuFakt.setIdPoreza(tempPorez);
				tempObracunatPorezZaStavkuFakt.setIznos(temp.getIznosPdvA());
				obracunatiPoreziBean.create(tempObracunatPorezZaStavkuFakt);
			}
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			StavkeFakture temp = bean.find(id);
			
			//azuriranje tabele faktura
			Faktura tempFakt = temp.getIdFakture();
			float ukupanRabat = tempFakt.getUkupanRabat() - temp.getOsnovica()*temp.getRabat()/100;
			float ukupanIznosBezPDV = tempFakt.getUkupanIznosBezPdvA() - temp.getOsnovica();
			float ukupanPDV = tempFakt.getUkupanPdv() - temp.getIznosPdvA();
			float ukupnoZaPlacanje = tempFakt.getUkupnoZaPlacanje() - (temp.getOsnovica() - temp.getOsnovica()*temp.getRabat()/100 + temp.getIznosPdvA());
			tempFakt.setUkupanRabat(ukupanRabat);
			tempFakt.setUkupanIznosBezPdvA(ukupanIznosBezPDV);
			tempFakt.setUkupanPdv(ukupanPDV);
			tempFakt.setUkupnoZaPlacanje(ukupnoZaPlacanje);
			fakturaBean.edit(tempFakt);
			
			//azuriranje tabele obracunati porezi
			Porez tempPorez = (temp.getIdProizvoda()).getIdGrupe().getIdPoreza();
			ObracunatiPorezi tempObracunatPorezZaStavkuFakt = null;
			for (ObracunatiPorezi x : obracunatiPoreziBean.findAll()) {
				if (x.getIdFakture().getIdFakture().intValue() == tempFakt.getIdFakture().intValue() &&
						x.getIdPoreza().getIdPoreza().intValue() == tempPorez.getIdPoreza().intValue()) {
					tempObracunatPorezZaStavkuFakt = x;
					break;
				}
			}
			if (tempObracunatPorezZaStavkuFakt != null) {
				tempObracunatPorezZaStavkuFakt.setIznos(tempObracunatPorezZaStavkuFakt.getIznos() - temp.getIznosPdvA());
				if (tempObracunatPorezZaStavkuFakt.getIznos() > 0) {
					obracunatiPoreziBean.edit(tempObracunatPorezZaStavkuFakt);
				} else { 
					obracunatiPoreziBean.remove(tempObracunatPorezZaStavkuFakt);
				}
			} else {
				System.out.println("mora postojati obracunati porez za stavku fakture koja se brise!");
			}
			
			
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(StavkeFaktureTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkeFakture temp = bean.find(param.idStavke);
			
			Faktura staraFaktura = temp.getIdFakture();
			Porez stariPorez = (temp.getIdProizvoda()).getIdGrupe().getIdPoreza();
			//PODRAZUMEVANO JE DA SE NE MENJA FAKTURA I POREZ STAVKE FAKTURE
			float stari_ukupanRabat = temp.getOsnovica()*temp.getRabat()/100;
			float stari_ukupanIznosBezPDV = temp.getOsnovica();
			float stari_ukupanPDV = temp.getIznosPdvA();
			float stari_ukupnoZaPlacanje = (temp.getOsnovica() - temp.getOsnovica()*temp.getRabat()/100 + temp.getIznosPdvA());
			
			
			//azuriranje tabele faktura
			float ukupanRabat = staraFaktura.getUkupanRabat() - stari_ukupanRabat + param.osnovica*param.rabat/100;
			float ukupanIznosBezPDV = staraFaktura.getUkupanIznosBezPdvA() - stari_ukupanIznosBezPDV + param.osnovica;
			float ukupanPDV = staraFaktura.getUkupanPdv() - stari_ukupanPDV + param.iznosPdvA;
			float ukupnoZaPlacanje = staraFaktura.getUkupnoZaPlacanje() - stari_ukupnoZaPlacanje + (param.osnovica - param.osnovica*param.rabat/100 + param.iznosPdvA);
			staraFaktura.setUkupanRabat(ukupanRabat);
			staraFaktura.setUkupanIznosBezPdvA(ukupanIznosBezPDV);
			staraFaktura.setUkupanPdv(ukupanPDV);
			staraFaktura.setUkupnoZaPlacanje(ukupnoZaPlacanje);
			fakturaBean.edit(staraFaktura);
			
			//azuriranje tabele obracunati porezi
			ObracunatiPorezi tempObracunatPorezZaStavkuFakt = null;
			for (ObracunatiPorezi x : obracunatiPoreziBean.findAll()) {
				if (x.getIdFakture().getIdFakture().intValue() == staraFaktura.getIdFakture().intValue() &&
						x.getIdPoreza().getIdPoreza().intValue() == stariPorez.getIdPoreza().intValue()) {
					tempObracunatPorezZaStavkuFakt = x;
					break;
				}
			}
			if (tempObracunatPorezZaStavkuFakt != null) {
				tempObracunatPorezZaStavkuFakt.setIznos(tempObracunatPorezZaStavkuFakt.getIznos() - stari_ukupanPDV + param.iznosPdvA);
				if (tempObracunatPorezZaStavkuFakt.getIznos() > 0) {
					obracunatiPoreziBean.edit(tempObracunatPorezZaStavkuFakt);
				} else { 
					obracunatiPoreziBean.remove(tempObracunatPorezZaStavkuFakt);
				}
			} else {
				System.out.println("mora postojati obracunati porez za stavku fakture koja se menja!");
			}
			
			
			temp.setIdProizvoda(proizvodBean.find(param.idProizvoda));
			temp.setIdFakture(fakturaBean.find(param.idFakture));
			temp.setIznosPdvA(param.iznosPdvA);
			temp.setJedinicnaCena(param.jedinicnaCena);
			temp.setKolicina(param.kolicina);
			temp.setOsnovica(param.osnovica);
			temp.setRabat(param.rabat);
			temp.setStopaPdvA(param.stopaPdvA);
			temp.setUkupanIznos(param.ukupanIznos);
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
}
