package projekat.poslovna.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Narudzbenica;
import projekat.poslovna.entity.Proizvod;
import projekat.poslovna.entity.StavkaNarudzbenice;
import projekat.poslovna.session.NarudzbenicaFacadeLocal;
import projekat.poslovna.session.ProizvodFacadeLocal;
import projekat.poslovna.session.StavkaNarudzbeniceFacadeLocal;
import projekat.poslovna.transport.StavkaNarudzbeniceTransportObject;

@Path("/stavka_narudzbenice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StavkaNarudzbeniceRest {
	
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	
	@EJB
	StavkaNarudzbeniceFacadeLocal bean;
	
	@GET
	public List<StavkaNarudzbeniceTransportObject> getAll(){
		return StavkaNarudzbeniceTransportObject.getListOfDtos(bean.findAll());
	}
	
	@GET
	@Path("/{id}")
	public StavkaNarudzbeniceTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
		StavkaNarudzbeniceTransportObject temp = new StavkaNarudzbeniceTransportObject(bean.find(id));
		return temp;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	@EJB
	NarudzbenicaFacadeLocal narudzbenicaBean;
	@EJB
	ProizvodFacadeLocal proizvodBean;
	
	@POST
	public void create(StavkaNarudzbeniceTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkaNarudzbenice tempStavka = new StavkaNarudzbenice(param.kolicinaStavkeNar, param.jedinicnaCenaStavkeNar);
			Proizvod tempProizvod  = proizvodBean.find(param.idProizvoda);
			Narudzbenica tempNarudzbenica = narudzbenicaBean.find(param.idNarudzbenice);
			tempStavka.setIdNarudzbenice(tempNarudzbenica);
			tempStavka.setIdProizvoda(tempProizvod);
			bean.create(tempStavka);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int param, @Context final HttpServletResponse response){
		try{
			StavkaNarudzbenice temp = bean.find(param);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@PUT
	public void update(StavkaNarudzbeniceTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkaNarudzbenice temp = bean.find(param.idStavkeNarudzbenice);
			temp.setIdProizvoda(proizvodBean.find(param.idProizvoda));
			temp.setIdNarudzbenice(narudzbenicaBean.find(param.idNarudzbenice));
			temp.setJedinicnaCenaStavkeNar(param.jedinicnaCenaStavkeNar);
			temp.setKolicinaStavkeNar(param.kolicinaStavkeNar);
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
}
