package projekat.poslovna.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.ObracunatiPorezi;
import projekat.poslovna.session.FakturaFacadeLocal;
import projekat.poslovna.session.ObracunatiPoreziFacadeLocal;
import projekat.poslovna.session.PorezFacadeLocal;
import projekat.poslovna.transport.ObracunatiPoreziTransportObject;

@Path("/obracunati_porezi")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ObracunatiPoreziRest {

	@EJB
	ObracunatiPoreziFacadeLocal bean;
	
	@GET
	public List<ObracunatiPoreziTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return ObracunatiPoreziTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public ObracunatiPoreziTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new ObracunatiPoreziTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@EJB
	FakturaFacadeLocal fakturaBean;
	@EJB
	PorezFacadeLocal porezBean;
	
	@POST
	public void create(ObracunatiPoreziTransportObject param, @Context final HttpServletResponse response){
		try{
			ObracunatiPorezi temp = new ObracunatiPorezi(param.iznos);
			
			temp.setIdFakture(fakturaBean.find(param.idFakture));
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			ObracunatiPorezi temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(ObracunatiPoreziTransportObject param, @Context final HttpServletResponse response){
		try{
			ObracunatiPorezi temp = bean.find(param.idObracunatogPoreza);
			temp.setIdFakture(fakturaBean.find(param.idFakture));
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			temp.setIznos(param.iznos);
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
}
