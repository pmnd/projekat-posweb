package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Cenovnik;
import projekat.poslovna.entity.GrupaProizvoda;
import projekat.poslovna.entity.PoslovnaGodina;
import projekat.poslovna.entity.PoslovniPartner;
import projekat.poslovna.entity.Preduzece;
import projekat.poslovna.entity.RacunUBanci;
import projekat.poslovna.session.PoslovniPartnerFacadeLocal;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.transport.CenovnikTransportObject;
import projekat.poslovna.transport.GrupaProizvodaTransportObject;
import projekat.poslovna.transport.PoslovnaGodinaTransportObject;
import projekat.poslovna.transport.PoslovniPartnerTransportObject;
import projekat.poslovna.transport.PreduzeceTransportObject;
import projekat.poslovna.transport.RacunUBanciTransportObject;

@Path("/preduzece")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreduzeceRest {

	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	
	@EJB
	PreduzeceFacadeLocal bean;
	
	@GET
	public List<PreduzeceTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return PreduzeceTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	
	@GET
	@Path("/{id}")
	public PreduzeceTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new PreduzeceTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@EJB
	PoslovniPartnerFacadeLocal poslovniPartnerBean;
	
	@POST
	public void create(PreduzeceTransportObject param, @Context final HttpServletResponse response){
		try{
			Preduzece temp = new Preduzece(param.nazivPreduzeca, param.adresa, param.pib);
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id")int param, @Context final HttpServletResponse response){
		try{
			Preduzece temp = bean.find(param);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@PUT
	public void update(PreduzeceTransportObject param, @Context final HttpServletResponse response){
		try{
			Preduzece temp = bean.find(param.idPreduzeca);
			temp.setAdresa(param.adresa);
			temp.setNazivPreduzeca(param.nazivPreduzeca);
			temp.setPib(param.pib);
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@GET
	@Path("/{id}/racun_u_banci")
	public List<RacunUBanciTransportObject> getSpecifiedRacuniUBanci(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return RacunUBanciTransportObject.getListOfDtos(new ArrayList<RacunUBanci>());
			}
			Set<RacunUBanci> listaStavki = bean.find(id).getRacunUBanciSet();
			return RacunUBanciTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/cenovnik")
	public List<CenovnikTransportObject> getSpecifiedCenovnici(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return CenovnikTransportObject.getListOfDtos(new ArrayList<Cenovnik>());
			}
			Set<Cenovnik> listaStavki = bean.find(id).getCenovnikSet();
			return CenovnikTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/grupa_proizvoda")
	public List<GrupaProizvodaTransportObject> getSpecifiedGrupePoreza(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return GrupaProizvodaTransportObject.getListOfDtos(new ArrayList<GrupaProizvoda>());
			}
			Set<GrupaProizvoda> listaStavki = bean.find(id).getGrupaProizvodaSet();
			return GrupaProizvodaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/poslovna_godina")
	public List<PoslovnaGodinaTransportObject> getSpecifiedPoslovneGodine(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return PoslovnaGodinaTransportObject.getListOfDtos(new ArrayList<PoslovnaGodina>());
			}
			Set<PoslovnaGodina> listaStavki = bean.find(id).getPoslovnaGodinaSet();
			return PoslovnaGodinaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/poslovni_partner")
	public List<PoslovniPartnerTransportObject> getSpecifiedPoslovniPartner(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return PoslovniPartnerTransportObject.getListOfDtos(new ArrayList<PoslovniPartner>());
			}
			Set<PoslovniPartner> listaStavki = bean.find(id).getPoslovniPartnerSet();
			return PoslovniPartnerTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
}
