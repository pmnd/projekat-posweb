package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.Narudzbenica;
import projekat.poslovna.entity.StavkaNarudzbenice;
import projekat.poslovna.session.NarudzbenicaFacadeLocal;
import projekat.poslovna.session.PoslovnaGodinaFacadeLocal;
import projekat.poslovna.session.PoslovniPartnerFacadeLocal;
import projekat.poslovna.transport.FakturaTransportObject;
import projekat.poslovna.transport.NarudzbenicaTransportObject;
import projekat.poslovna.transport.StavkaNarudzbeniceTransportObject;

@Path("/narudzbenica")
public class NarudzbenicaRest {
	
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	@EJB
	NarudzbenicaFacadeLocal bean;
	@EJB
	PoslovniPartnerFacadeLocal poslovniPartnerBean;
	
	@GET
	public List<NarudzbenicaTransportObject> getAll(){
		return NarudzbenicaTransportObject.getListOfDtos(bean.findAll());
	}
	
	@GET
	@Path("/{id}")
	public NarudzbenicaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new NarudzbenicaTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	
	
	@EJB
	PoslovnaGodinaFacadeLocal poslovnaGodinaBean;
	@POST
	public void create(NarudzbenicaTransportObject param, @Context final HttpServletResponse response){
		try{
			Narudzbenica temp = new Narudzbenica(param.brojNarudzbenice, param.datum, param.prihvacena);
			temp.setIdGodine(poslovnaGodinaBean.find(param.idGodine));
			temp.setIdPoslovnogPartnera(poslovniPartnerBean.find(param.idPoslovnogPartnera));
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id")	int param, @Context final HttpServletResponse response){
		try{
			Narudzbenica temp = bean.find(param);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@PUT
	public void update(NarudzbenicaTransportObject param, @Context final HttpServletResponse response){
		try{
			Narudzbenica temp = bean.find(param.idNarudzbenice);
			temp.setBrojNarudzbenice(param.brojNarudzbenice);
			temp.setDatum(param.datum);
			temp.setPrihvacena(param.prihvacena);
			temp.setIdGodine(poslovnaGodinaBean.find(param.idGodine));
			temp.setIdPoslovnogPartnera(poslovniPartnerBean.find(param.idPoslovnogPartnera));
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	
	@GET
	@Path("/{id}/faktura")
	public List<FakturaTransportObject> getSpecifiedFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return FakturaTransportObject.getListOfDtos(new ArrayList<Faktura>());
			}
			Set<Faktura> listaStavki = bean.find(id).getFakturaSet();
			return FakturaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/stavka_narudzbenice")
	public List<StavkaNarudzbeniceTransportObject> getSpecifiedStavkeNarudzbenice(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkaNarudzbeniceTransportObject.getListOfDtos(new ArrayList<StavkaNarudzbenice>());
			}
			Set<StavkaNarudzbenice> listaStavki = bean.find(id).getStavkaNarudzbeniceSet();
			return StavkaNarudzbeniceTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	//[KreiranjeFaktureOdNarudzbine]
	
	@GET
	@Path("/fakturaSP/{id}")
	public Integer createFakturaOdNarudzbenice(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (id < 0) {
				response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
				return null;
			}
			return bean.createFakturaSP(id);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}

}
