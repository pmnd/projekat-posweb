package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Cenovnik;
import projekat.poslovna.entity.StavkeCenovnika;
import projekat.poslovna.session.CenovnikFacadeLocal;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.transport.CenovnikStoredProcedureTranspObj;
import projekat.poslovna.transport.CenovnikTransportObject;
import projekat.poslovna.transport.StavkeCenovnikaTransportObject;

@Path("/cenovnik")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CenovnikRest {
	
	@EJB
	CenovnikFacadeLocal bean;
	
	@EJB
	PreduzeceFacadeLocal preduzeceBean;
	
	@GET
	public List<CenovnikTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return CenovnikTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	
	
	@GET
	@Path("/{id}")
	public CenovnikTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new CenovnikTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(CenovnikTransportObject param, @Context final HttpServletResponse response){
		try{
			Cenovnik temp = new Cenovnik(param.vaziOd, param.nazivCenovnika);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			Cenovnik temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	

	
	@PUT
	public void update(CenovnikTransportObject param, @Context final HttpServletResponse response){
		try{
			Cenovnik temp = bean.find(param.idCenovnika);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			temp.setVaziOd(param.vaziOd);
			temp.setNazivCenovnika(param.nazivCenovnika);
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/stavke_cenovnika")
	public List<StavkeCenovnikaTransportObject> getSpecifiedStavkeCenovnika(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkeCenovnikaTransportObject.getListOfDtos(new ArrayList<StavkeCenovnika>());
			}
			Set<StavkeCenovnika> listaStavki = bean.find(id).getStavkeCenovnikaSet();
			return StavkeCenovnikaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
	//[KopiranjeCenovnika]
	
	@POST
	@Path("/cenovnikSP")
	public Integer kopirajCenovnik(CenovnikStoredProcedureTranspObj param, @Context final HttpServletResponse response){
		try{
			if (param.idStarogCenovnika == null || param.nazivCenovnika == null ||
					param.procenat == 0 || param.vaziOd == null) {
				response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
				return null;
			}
			return bean.kopiranjeCenovnikaSP(param.idStarogCenovnika, param.nazivCenovnika, param.vaziOd, param.procenat);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	

}
