package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.RacunUBanci;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.session.RacunUBanciFacadeLocal;
import projekat.poslovna.transport.FakturaTransportObject;
import projekat.poslovna.transport.RacunUBanciTransportObject;

@Path("/racun_u_banci")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RacunUBanciRest {
	@EJB
	RacunUBanciFacadeLocal bean;
	
	@GET
	public List<RacunUBanciTransportObject> getAll(){
		return RacunUBanciTransportObject.getListOfDtos(bean.findAll());
	}
	
	@GET
	@Path("/{id}")
	public RacunUBanciTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new RacunUBanciTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	@EJB
	PreduzeceFacadeLocal preduzeceBean;
	
	@POST
	public void create(RacunUBanciTransportObject param, @Context final HttpServletResponse response){
		try{
			RacunUBanci temp = new RacunUBanci(param.brojRacuna, param.nazivBanke);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			RacunUBanci temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(RacunUBanciTransportObject param, @Context final HttpServletResponse response){
		try{
			RacunUBanci temp = bean.find(param.idRacuna);
			temp.setBrojRacuna(param.brojRacuna);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			temp.setNazivBanke(param.nazivBanke);
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/faktura")
	public List<FakturaTransportObject> getSpecifiedFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return FakturaTransportObject.getListOfDtos(new ArrayList<Faktura>());
			}
			Set<Faktura> listaStavki = bean.find(id).getFakture();
			return FakturaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
}
