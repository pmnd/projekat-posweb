package projekat.poslovna.rest;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.ObracunatiPorezi;
import projekat.poslovna.entity.StavkeFakture;
import projekat.poslovna.session.FakturaFacadeLocal;
import projekat.poslovna.session.NarudzbenicaFacadeLocal;
import projekat.poslovna.session.PoslovnaGodinaFacadeLocal;
import projekat.poslovna.session.PoslovniPartnerFacadeLocal;
import projekat.poslovna.session.RacunUBanciFacadeLocal;
import projekat.poslovna.transport.CleanDTO;
import projekat.poslovna.transport.FakturaTransportObject;
import projekat.poslovna.transport.ObracunatiPoreziTransportObject;
import projekat.poslovna.transport.StavkeFaktureTransportObject;
import projekat.poslovna.xml.FakturaXML;

@Path("/faktura")
public class FakturaRest {
	@EJB
	FakturaFacadeLocal bean;
	@EJB
	PoslovnaGodinaFacadeLocal poslovnaGodinaBean;
	
	@EJB
	PoslovniPartnerFacadeLocal partnerBean;
	
	@EJB
	NarudzbenicaFacadeLocal narudzbenicaBean;
	


	@EJB
	RacunUBanciFacadeLocal racunBean;
	
	@GET
	public List<FakturaTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			
			return FakturaTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public FakturaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new FakturaTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(FakturaTransportObject param, @Context final HttpServletResponse response){
		try{
			Faktura temp = new Faktura(param.brojFakture, param.datumFakture, param.datumPlacanjaFakture, param.ukupanRabat, 
					param.ukupanIznosBezPdvA, param.ukupanPdv, param.ukupnoZaPlacanje, param.status);
			
			temp.setIdGodine(poslovnaGodinaBean.find(param.idGodine));
			temp.setIdPoslovnogPartnera(partnerBean.find(param.idPoslovnogPartnera));
			temp.setIdNarudzbenice(narudzbenicaBean.find(param.idNarudzbenice));
			temp.setIdRacunaUBanci(racunBean.find(param.idRacuna));
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			Faktura temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(FakturaTransportObject param, @Context final HttpServletResponse response){
		try{
			Faktura temp = bean.find(param.idFakture);
			temp.setBrojFakture(param.brojFakture);
			temp.setDatumFakture(param.datumFakture);
			temp.setDatumPlacanjaFakture(param.datumPlacanjaFakture);
			temp.setStatus(param.status);
			temp.setUkupanIznosBezPdvA(param.ukupanIznosBezPdvA);
			temp.setUkupanPdv(param.ukupanPdv);
			temp.setUkupanRabat(param.ukupanRabat);
			temp.setUkupnoZaPlacanje(param.ukupnoZaPlacanje);
			temp.setIdGodine(poslovnaGodinaBean.find(param.idGodine));
			temp.setIdNarudzbenice(narudzbenicaBean.find(param.idNarudzbenice));
			temp.setIdPoslovnogPartnera(partnerBean.find(param.idPoslovnogPartnera));
			temp.setIdRacunaUBanci(racunBean.find(param.idRacuna));
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	
	@GET
	@Path("/{id}/stavke_fakture")
	public List<StavkeFaktureTransportObject> getSpecifiedStavkeFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkeFaktureTransportObject.getListOfDtos(new ArrayList<StavkeFakture>());
			}
			Set<StavkeFakture> listaStavki = bean.find(id).getStavkeFaktureSet();
			return StavkeFaktureTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/obracunati_porezi")
	public List<ObracunatiPoreziTransportObject> getSpecifiedObracunatiPorezi(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return ObracunatiPoreziTransportObject.getListOfDtos(new ArrayList<ObracunatiPorezi>());
			}
			Set<ObracunatiPorezi> listaStavki = bean.find(id).getObracunatiPoreziSet();
			return ObracunatiPoreziTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/exportXML/{id}")
	public CleanDTO exportXML(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			Faktura tempFaktura = bean.find(id);
			FakturaXML xmlFaktura = new FakturaXML(tempFaktura);
			
			JAXBContext contextFaktura = JAXBContext.newInstance(FakturaXML.class);
			Marshaller marshallFaktura = contextFaktura.createMarshaller();
			StringWriter sw = new StringWriter();
			marshallFaktura.marshal(xmlFaktura, sw);
			String xmlString = sw.toString();
			return new CleanDTO(xmlString);
		}catch(Exception e){
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
}
