package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.Narudzbenica;
import projekat.poslovna.entity.PoslovniPartner;
import projekat.poslovna.session.PoslovniPartnerFacadeLocal;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.transport.FakturaTransportObject;
import projekat.poslovna.transport.NarudzbenicaTransportObject;
import projekat.poslovna.transport.PoslovniPartnerTransportObject;

@Path("/poslovni_partner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PoslovniPartnerRest {
	@EJB
	PoslovniPartnerFacadeLocal bean;
	@EJB
	PreduzeceFacadeLocal preduzeceBean;
	
	@GET
	public List<PoslovniPartnerTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return PoslovniPartnerTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public PoslovniPartnerTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new PoslovniPartnerTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(PoslovniPartnerTransportObject param, @Context final HttpServletResponse response){
		try{
			PoslovniPartner temp = new PoslovniPartner(param.naziv, param.adresa, param.pib, param.idPoslovnogPartnera, param.vrsta);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			
			bean.create(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			PoslovniPartner temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(PoslovniPartnerTransportObject param, @Context final HttpServletResponse response){
		try{
			PoslovniPartner temp = bean.find(param.idPoslovnogPartnera);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			temp.setAdresa(param.adresa);
			temp.setIdPoslovnogPartnera(param.idPoslovnogPartnera);
			temp.setVrsta(param.vrsta);
			temp.setPib(param.pib);
			temp.setNaziv(param.naziv);
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/narudzbenica")
	public List<NarudzbenicaTransportObject> getSpecifiedNarudzbenice(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return NarudzbenicaTransportObject.getListOfDtos(new ArrayList<Narudzbenica>());
			}
			Set<Narudzbenica> listaStavki = bean.find(id).getNarudzbenicaSet();
			return NarudzbenicaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/faktura")
	public List<FakturaTransportObject> getSpecifiedFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return FakturaTransportObject.getListOfDtos(new ArrayList<Faktura>());
			}
			Set<Faktura> listaStavki = bean.find(id).getFakturaSet();
			return FakturaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
}
