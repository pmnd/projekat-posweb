package projekat.poslovna.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.StopaPoreza;
import projekat.poslovna.session.PorezFacadeLocal;
import projekat.poslovna.session.StopaPorezaFacadeLocal;
import projekat.poslovna.transport.StopaPorezaTransportObject;

@Path("/stopa_poreza")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StopaPorezaRest {
	@EJB
	StopaPorezaFacadeLocal bean;
	@EJB
	PorezFacadeLocal porezBean;
	
	
	@GET
	public List<StopaPorezaTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return StopaPorezaTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public StopaPorezaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new StopaPorezaTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(StopaPorezaTransportObject param, @Context final HttpServletResponse response){
		try{
			StopaPoreza temp = new StopaPoreza(param.stopa,param.datumVazenja);
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			StopaPoreza temp = bean.find(id);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			bean.remove(temp);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(StopaPorezaTransportObject param, @Context final HttpServletResponse response){
		try{
			StopaPoreza temp = bean.find(param.idStope);
			temp.setDatumVazenja(param.datumVazenja);
			temp.setStopa(param.stopa);
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			
			bean.edit(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
}
