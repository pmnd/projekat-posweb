package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.JedinicaMere;
import projekat.poslovna.entity.Proizvod;
import projekat.poslovna.session.JedinicaMereFacadeLocal;
import projekat.poslovna.transport.ProizvodTransportObject;

@Path("/jedinica_mere")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JedinicaMereRest {
		
	@EJB
	JedinicaMereFacadeLocal jedinicaMereBean;
	
	@GET
	public List<JedinicaMere> getAll(){
		return jedinicaMereBean.findAll();
	}
	
	@GET
	@Path("/{id}")
	public JedinicaMere getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return jedinicaMereBean.find(id);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(JedinicaMere param, @Context final HttpServletResponse response){
		try{
			jedinicaMereBean.create(param);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			jedinicaMereBean.remove(jedinicaMereBean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(JedinicaMere param, @Context final HttpServletResponse response){
		try{
			JedinicaMere jm = jedinicaMereBean.find(param.getIdJedinice());
			jm.setNazivJediniceMere(param.getNazivJediniceMere());
			jm.setSkracenica(param.getSkracenica());
			jedinicaMereBean.edit(jm);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	
	@GET
	@Path("/{id}/proizvod")
	public List<ProizvodTransportObject> getSpecifiedProizvodi(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (jedinicaMereBean.find(id) == null) {
				return ProizvodTransportObject.getListOfDtos(new ArrayList<Proizvod>());
			}
			Set<Proizvod> listaStavki = jedinicaMereBean.find(id).getProizvodSet();
			return ProizvodTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
	
		
}
