package projekat.poslovna.rest;

import java.util.Random;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import projekat.poslovna.entity.Korisnik;
import projekat.poslovna.session.KorisnikFacadeLocal;
import projekat.poslovna.transport.KorisnikTransportObject;
import projekat.poslovna.transport.LoginDataTO;

@Path("/korisnik")
public class KorisnikRest {

	@Context HttpServletRequest request;
	@EJB
	KorisnikFacadeLocal bean;
	
	@POST
	@Path("/login")
	public LoginDataTO login(KorisnikTransportObject korisnik, @Context final HttpServletResponse response){
		System.out.println(korisnik.username + korisnik.password);
		try{
			Korisnik k = bean.findByUsername(korisnik.username);
			String token;
			Random rn  = new Random();
			if(k == null){
				response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
				return null;
			}else{
				if(k.getPassword().equals(korisnik.password)){
					token = ""+rn.nextLong() + korisnik.username + rn.nextInt() + "dhkjdhkjasdh" + rn.nextLong();
					if(k.getToken().equals("")){
						k.setToken(token);
						bean.edit(k);
					}else
						token = k.getToken();
					
					response.setStatus(HttpServletResponse.SC_OK);
					LoginDataTO temp = new  LoginDataTO();
					temp.token = token;
					temp.username = korisnik.username;
					return temp;
				}
				
			}
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
		


		response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
		return null;
	}
	

	@POST
	@Path("/logout")
	public void logout(@Context final HttpServletResponse response){
		Korisnik k = null;
		try{
			k = bean.findByToken(request.getHeader("Authorization"));
		}catch(Exception e){
			
		}
		
		if(k == null){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}else{
			
			response.setStatus(HttpServletResponse.SC_OK);
			response.setHeader("Authorization", "");
			return;
			
		}
	}
	
	
	
	
	
	
	
	
}
