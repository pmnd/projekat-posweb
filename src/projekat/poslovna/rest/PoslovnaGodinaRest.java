package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.Narudzbenica;
import projekat.poslovna.entity.PoslovnaGodina;
import projekat.poslovna.session.PoslovnaGodinaFacadeLocal;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.transport.FakturaTransportObject;
import projekat.poslovna.transport.NarudzbenicaTransportObject;
import projekat.poslovna.transport.PoslovnaGodinaTransportObject;

@Path("/poslovna_godina")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PoslovnaGodinaRest {
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	@EJB
	PoslovnaGodinaFacadeLocal bean;
	
	@GET
	public List<PoslovnaGodinaTransportObject> getAll(){
		return PoslovnaGodinaTransportObject.getListOfDtos(bean.findAll());
	}
	
	@GET
	@Path("/{id}")
	public PoslovnaGodinaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			PoslovnaGodina tempPG = bean.find(id);
			return new PoslovnaGodinaTransportObject(tempPG);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
	}
	
	@EJB 
	PreduzeceFacadeLocal preduzeceBean;
	
	@POST
	public void create(PoslovnaGodinaTransportObject param, @Context final HttpServletResponse response){
		try{
			PoslovnaGodina temp = new PoslovnaGodina(param.godina);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			bean.create(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int param, @Context final HttpServletResponse response){
		try{
			PoslovnaGodina temp = bean.find(param);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(PoslovnaGodinaTransportObject param, @Context final HttpServletResponse response){
		try{
			PoslovnaGodina temp = bean.find(param.idGodine);
			temp.setDaLiJeZakljucena(param.daLiJeZakljucena);
			temp.setGodina(param.godina);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/narudzbenica")
	public List<NarudzbenicaTransportObject> getSpecifiedNarudzbenice(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return NarudzbenicaTransportObject.getListOfDtos(new ArrayList<Narudzbenica>());
			}
			Set<Narudzbenica> listaStavki = bean.find(id).getNarudzbenicaSet();
			return NarudzbenicaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/faktura")
	public List<FakturaTransportObject> getSpecifiedFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return FakturaTransportObject.getListOfDtos(new ArrayList<Faktura>());
			}
			Set<Faktura> listaStavki = bean.find(id).getFakturaSet();
			return FakturaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
}
