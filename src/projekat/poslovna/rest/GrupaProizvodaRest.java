package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;

import projekat.poslovna.entity.GrupaProizvoda;
import projekat.poslovna.entity.Proizvod;
import projekat.poslovna.session.GrupaProizvodaFacadeLocal;
import projekat.poslovna.session.PorezFacadeLocal;
import projekat.poslovna.session.PreduzeceFacadeLocal;
import projekat.poslovna.transport.GrupaProizvodaTransportObject;
import projekat.poslovna.transport.ProizvodTransportObject;

@Path("/grupa_proizvoda")
public class GrupaProizvodaRest {
	@EJB
	GrupaProizvodaFacadeLocal bean;
	@EJB
	PreduzeceFacadeLocal preduzeceBean;
	@EJB
	PorezFacadeLocal porezBean;
	
	
	@GET
	public List<GrupaProizvodaTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return GrupaProizvodaTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public GrupaProizvodaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new GrupaProizvodaTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(GrupaProizvodaTransportObject param, @Context final HttpServletResponse response){
		try{
			GrupaProizvoda temp = new GrupaProizvoda(param.nazivGrupe);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			GrupaProizvoda temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(GrupaProizvodaTransportObject param, @Context final HttpServletResponse response){
		try{
			GrupaProizvoda temp = bean.find(param.idGrupe);
			temp.setNazivGrupe(param.nazivGrupe);
			temp.setIdPreduzeca(preduzeceBean.find(param.idPreduzeca));
			temp.setIdPoreza(porezBean.find(param.idPoreza));
			bean.edit(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/proizvod")
	public List<ProizvodTransportObject> getSpecifiedProizvodi(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return ProizvodTransportObject.getListOfDtos(new ArrayList<Proizvod>());
			}
			Set<Proizvod> listaStavki = bean.find(id).getProizvodSet();
			return ProizvodTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}

}
