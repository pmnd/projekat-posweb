package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.Proizvod;
import projekat.poslovna.entity.StavkaNarudzbenice;
import projekat.poslovna.entity.StavkeCenovnika;
import projekat.poslovna.entity.StavkeFakture;
import projekat.poslovna.session.GrupaProizvodaFacadeLocal;
import projekat.poslovna.session.JedinicaMereFacadeLocal;
import projekat.poslovna.session.ProizvodFacadeLocal;
import projekat.poslovna.transport.ProizvodTransportObject;
import projekat.poslovna.transport.StavkaNarudzbeniceTransportObject;
import projekat.poslovna.transport.StavkeCenovnikaTransportObject;
import projekat.poslovna.transport.StavkeFaktureTransportObject;

@Path("/proizvod")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProizvodRest {
	
	@EJB
	ProizvodFacadeLocal bean;
	@EJB
	GrupaProizvodaFacadeLocal grupaBean;
	@EJB
	JedinicaMereFacadeLocal jedinicaBean;
	
	@GET
	public List<ProizvodTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return ProizvodTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public ProizvodTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new ProizvodTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(ProizvodTransportObject param, @Context final HttpServletResponse response){
		try{
			Proizvod temp = new Proizvod(param.nazivProizvoda, param.jedinicnaKolicina);
			
			temp.setIdGrupe(grupaBean.find(param.idGrupe));
			temp.setIdJedinice(jedinicaBean.find(param.idJedinice));
			
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			Proizvod temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(ProizvodTransportObject param, @Context final HttpServletResponse response){
		try{
			Proizvod temp = bean.find(param.idProizvoda);
			temp.setIdGrupe(grupaBean.find(param.idGrupe));
			temp.setIdJedinice(jedinicaBean.find(param.idJedinice));
			temp.setJedinicnaKolicina(param.jedinicnaKolicina);
			temp.setNazivProizvoda(param.nazivProizvoda);
			
			bean.edit(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/stavke_fakture")
	public List<StavkeFaktureTransportObject> getSpecifiedStavkeFakture(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkeFaktureTransportObject.getListOfDtos(new ArrayList<StavkeFakture>());
			}
			Set<StavkeFakture> listaStavki = bean.find(id).getStavkeFaktureSet();
			return StavkeFaktureTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/stavke_cenovnika")
	public List<StavkeCenovnikaTransportObject> getSpecifiedStavkeCenovnika(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkeCenovnikaTransportObject.getListOfDtos(new ArrayList<StavkeCenovnika>());
			}
			Set<StavkeCenovnika> listaStavki = bean.find(id).getStavkeCenovnikaSet();
			return StavkeCenovnikaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/stavka_narudzbenice")
	public List<StavkaNarudzbeniceTransportObject> getSpecifiedStavkeNarudzbenice(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StavkaNarudzbeniceTransportObject.getListOfDtos(new ArrayList<StavkaNarudzbenice>());
			}
			Set<StavkaNarudzbenice> listaStavki = bean.find(id).getStavkaNarudzbeniceSet();
			return StavkaNarudzbeniceTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
}
