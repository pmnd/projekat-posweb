package projekat.poslovna.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.GrupaProizvoda;
import projekat.poslovna.entity.ObracunatiPorezi;
import projekat.poslovna.entity.Porez;
import projekat.poslovna.entity.StopaPoreza;
import projekat.poslovna.session.PorezFacadeLocal;
import projekat.poslovna.transport.GrupaProizvodaTransportObject;
import projekat.poslovna.transport.ObracunatiPoreziTransportObject;
import projekat.poslovna.transport.StopaPorezaTransportObject;


@Path("/porez")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PorezRest {
	@EJB
	PorezFacadeLocal bean;
	
	@GET
	public List<Porez> getAll(){
		return bean.findAll();
	}
	
	@GET
	@Path("/{id}")
	public Porez getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return bean.find(id);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(Porez param, @Context final HttpServletResponse response){
		try{
			bean.create(param);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			bean.remove(bean.find(id));
			
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(Porez param, @Context final HttpServletResponse response){
		try{
			Porez temp = bean.find(param.getIdPoreza());
			temp.setNazivPoreza(param.getNazivPoreza());
			bean.edit(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@GET
	@Path("/{id}/stopa_poreza")
	public List<StopaPorezaTransportObject> getSpecifiedStopePoreza(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return StopaPorezaTransportObject.getListOfDtos(new ArrayList<StopaPoreza>());
			}
			Set<StopaPoreza> listaStavki = bean.find(id).getStopaPorezaSet();
			return StopaPorezaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/grupa_proizvoda")
	public List<GrupaProizvodaTransportObject> getSpecifiedGrupePoreza(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return GrupaProizvodaTransportObject.getListOfDtos(new ArrayList<GrupaProizvoda>());
			}
			Set<GrupaProizvoda> listaStavki = bean.find(id).getGrupaProizvodaSet();
			return GrupaProizvodaTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}/obracunati_porezi")
	public List<ObracunatiPoreziTransportObject> getSpecifiedObracunatiPorezi(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			if (bean.find(id) == null) {
				return ObracunatiPoreziTransportObject.getListOfDtos(new ArrayList<ObracunatiPorezi>());
			}
			Set<ObracunatiPorezi> listaStavki = bean.find(id).getObracunatiPoreziSet();
			return ObracunatiPoreziTransportObject.getListOfDtos(listaStavki);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	
}
