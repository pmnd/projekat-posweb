package projekat.poslovna.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import projekat.poslovna.entity.StavkeCenovnika;
import projekat.poslovna.session.CenovnikFacadeLocal;
import projekat.poslovna.session.ProizvodFacadeLocal;
import projekat.poslovna.session.StavkeCenovnikaFacadeLocal;
import projekat.poslovna.transport.StavkeCenovnikaTransportObject;

@Path("/stavke_cenovnika")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StavkeCenovnikaRest {
	@EJB
	StavkeCenovnikaFacadeLocal bean;
	@EJB
	CenovnikFacadeLocal cenovnikBean;
	@EJB
	ProizvodFacadeLocal proizvodBean;
	
	@GET
	public List<StavkeCenovnikaTransportObject> getAll(@Context final HttpServletResponse response){
		try{
			return StavkeCenovnikaTransportObject.getListOfDtos(bean.findAll());
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@GET
	@Path("/{id}")
	public StavkeCenovnikaTransportObject getOne(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			return new StavkeCenovnikaTransportObject(bean.find(id));
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return null;
		}
		
	}
	
	@POST
	public void create(StavkeCenovnikaTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkeCenovnika temp = new StavkeCenovnika(param.cena);
			temp.setIdCenovnika(cenovnikBean.find(param.idCenovnika));
			temp.setIdProizvoda(proizvodBean.find(param.idProizvoda));
			bean.create(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@DELETE
	@Path("/{id}")
	public void remove(@PathParam("id") int id, @Context final HttpServletResponse response){
		try{
			StavkeCenovnika temp = bean.find(id);
			bean.remove(temp);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
	
	@PUT
	public void update(StavkeCenovnikaTransportObject param, @Context final HttpServletResponse response){
		try{
			StavkeCenovnika temp = bean.find(param.idStavkeCenovnika);
			temp.setIdCenovnika(cenovnikBean.find(param.idCenovnika));
			temp.setIdProizvoda(proizvodBean.find(param.idProizvoda));
			temp.setCena(param.cena);
			bean.edit(temp);
			
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}catch(Exception e){
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		
	}
}
