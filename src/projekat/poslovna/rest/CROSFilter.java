package projekat.poslovna.rest;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CROSFilter implements Filter {

	    @Override
	    public void init(FilterConfig filterConfig) throws ServletException {

	    }

	    @Override
	    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
	        HttpServletResponse response = (HttpServletResponse) servletResponse;
	        HttpServletRequest request = (HttpServletRequest) servletRequest;
	        /*response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Credentials", "true");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE");
	        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");
	        */
	        String url = request.getRequestURI();
	        
	        if(url.startsWith("/POSLOVNA/rest")){
	        	System.out.println(url + "    " + request.getHeader("Authorization"));
	        	if(url.startsWith("/POSLOVNA/rest/korisnik/login") || url.startsWith("/POSLOVNA/rest/sema")){
	        		filterChain.doFilter(servletRequest, servletResponse);
	        	}else if(request.getHeader("Authorization") == "" || request.getHeader("Authorization") == null){
	        		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	        		return;
	        	}
	        	
	        }
	        
	        
	        
	        
	        
	        
	        
	        filterChain.doFilter(servletRequest, servletResponse);
	    }

	    @Override
	    public void destroy() {

	    }

	
}
