package projekat.poslovna.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateFormater extends XmlAdapter<String, Date> {

	private SimpleDateFormat formatOfTheDate = new SimpleDateFormat("yyyy-MM-dd");

	
	
    @Override
    public String marshal(Date v) throws Exception {
        return formatOfTheDate.format(v);
    }

    @Override
    public Date unmarshal(String v) throws Exception {
        return formatOfTheDate.parse(v);
    }

}
