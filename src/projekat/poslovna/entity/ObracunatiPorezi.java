/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "obracunati_porezi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ObracunatiPorezi.findAll", query = "SELECT o FROM ObracunatiPorezi o"),
    @NamedQuery(name = "ObracunatiPorezi.findByIznos", query = "SELECT o FROM ObracunatiPorezi o WHERE o.iznos = :iznos"),
    @NamedQuery(name = "ObracunatiPorezi.findByIdObracunatogPoreza", query = "SELECT o FROM ObracunatiPorezi o WHERE o.idObracunatogPoreza = :idObracunatogPoreza")})
public class ObracunatiPorezi implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "IZNOS")
    private float iznos;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_OBRACUNATOG_POREZA")
    private Integer idObracunatogPoreza;
    
    @JoinColumn(name = "ID_FAKTURE", referencedColumnName = "ID_FAKTURE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Faktura idFakture;
    
    @JoinColumn(name = "ID_POREZA", referencedColumnName = "ID_POREZA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Porez idPoreza;

    public ObracunatiPorezi() {
    }

  

    public ObracunatiPorezi(float iznos) {
        this.iznos = iznos;
    }

    public float getIznos() {
        return iznos;
    }

    public void setIznos(float iznos) {
        this.iznos = iznos;
    }

    public Integer getIdObracunatogPoreza() {
        return idObracunatogPoreza;
    }

    public void setIdObracunatogPoreza(Integer idObracunatogPoreza) {
        this.idObracunatogPoreza = idObracunatogPoreza;
    }

    public Faktura getIdFakture() {
        return idFakture;
    }

    public void setIdFakture(Faktura idFakture) {
        this.idFakture = idFakture;
    }

    public Porez getIdPoreza() {
        return idPoreza;
    }

    public void setIdPoreza(Porez idPoreza) {
        this.idPoreza = idPoreza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObracunatogPoreza != null ? idObracunatogPoreza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ObracunatiPorezi)) {
            return false;
        }
        ObracunatiPorezi other = (ObracunatiPorezi) object;
        if ((this.idObracunatogPoreza == null && other.idObracunatogPoreza != null) || (this.idObracunatogPoreza != null && !this.idObracunatogPoreza.equals(other.idObracunatogPoreza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ObracunatiPorezi[ idObracunatogPoreza=" + idObracunatogPoreza + " ]";
    }
    
}
