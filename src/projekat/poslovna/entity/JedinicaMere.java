/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "jedinica_mere")
@XmlRootElement(name="jedinica_mere")
@NamedQueries({
    @NamedQuery(name = "JedinicaMere.findAll", query = "SELECT j FROM JedinicaMere j"),
    @NamedQuery(name = "JedinicaMere.findByIdJedinice", query = "SELECT j FROM JedinicaMere j WHERE j.idJedinice = :idJedinice"),
    @NamedQuery(name = "JedinicaMere.findByNazivJediniceMere", query = "SELECT j FROM JedinicaMere j WHERE j.nazivJediniceMere = :nazivJediniceMere"),
    @NamedQuery(name = "JedinicaMere.findBySkracenica", query = "SELECT j FROM JedinicaMere j WHERE j.skracenica = :skracenica")})
public class JedinicaMere implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_JEDINICE")
    private Integer idJedinice;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "NAZIV_JEDINICE_MERE")
    private String nazivJediniceMere;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "SKRACENICA")
    private String skracenica;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idJedinice", fetch = FetchType.EAGER)
    private Set<Proizvod> proizvodSet;

    public JedinicaMere() {
    }


    public JedinicaMere(String nazivJediniceMere, String skracenica) {
        
        this.nazivJediniceMere = nazivJediniceMere;
        this.skracenica = skracenica;
    }

    public Integer getIdJedinice() {
        return idJedinice;
    }

    public void setIdJedinice(Integer idJedinice) {
        this.idJedinice = idJedinice;
    }

    public String getNazivJediniceMere() {
        return nazivJediniceMere;
    }

    public void setNazivJediniceMere(String nazivJediniceMere) {
        this.nazivJediniceMere = nazivJediniceMere;
    }

    public String getSkracenica() {
        return skracenica;
    }

    public void setSkracenica(String skracenica) {
        this.skracenica = skracenica;
    }

    @XmlTransient
    public Set<Proizvod> getProizvodSet() {
        return proizvodSet;
    }

    public void setProizvodSet(Set<Proizvod> proizvodSet) {
        this.proizvodSet = proizvodSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJedinice != null ? idJedinice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JedinicaMere)) {
            return false;
        }
        JedinicaMere other = (JedinicaMere) object;
        if ((this.idJedinice == null && other.idJedinice != null) || (this.idJedinice != null && !this.idJedinice.equals(other.idJedinice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.JedinicaMere[ idJedinice=" + idJedinice + " ]";
    }
    
}
