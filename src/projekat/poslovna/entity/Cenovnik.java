package projekat.poslovna.entity;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "cenovnik")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cenovnik.findAll", query = "SELECT c FROM Cenovnik c"),
    @NamedQuery(name = "Cenovnik.findByIdCenovnika", query = "SELECT c FROM Cenovnik c WHERE c.idCenovnika = :idCenovnika"),
    @NamedQuery(name = "Cenovnik.findByVaziOd", query = "SELECT c FROM Cenovnik c WHERE c.vaziOd = :vaziOd")})
public class Cenovnik implements Serializable {

    private static final long serialVersionUID = 1L;
     
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CENOVNIKA")
    private Integer idCenovnika;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "NAZIV_CENOVNIKA")
    private String nazivCenovnika;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "VAZI_OD")
    @Temporal(TemporalType.DATE)
    private Date vaziOd;
    
    
    @JoinColumn(name = "ID_PREDUZECA", referencedColumnName = "ID_PREDUZECA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Preduzece idPreduzeca;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCenovnika", fetch = FetchType.EAGER)
    private Set<StavkeCenovnika> stavkeCenovnikaSet;

    public Cenovnik() {
    }


    public Cenovnik(Date vaziOd, String nazivCenovnika) {
        this.vaziOd = vaziOd;
        this.nazivCenovnika = nazivCenovnika;
    }

    public Integer getIdCenovnika() {
        return idCenovnika;
    }

    public void setIdCenovnika(Integer idCenovnika) {
        this.idCenovnika = idCenovnika;
    }

    public Date getVaziOd() {
        return vaziOd;
    }

    public void setVaziOd(Date vaziOd) {
        this.vaziOd = vaziOd;
    }

    public Preduzece getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Preduzece idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }

    @XmlTransient
    public Set<StavkeCenovnika> getStavkeCenovnikaSet() {
        return stavkeCenovnikaSet;
    }

    public void setStavkeCenovnikaSet(Set<StavkeCenovnika> stavkeCenovnikaSet) {
        this.stavkeCenovnikaSet = stavkeCenovnikaSet;
    }
    
    public String getNazivCenovnika() {
		return nazivCenovnika;
	}


	public void setNazivCenovnika(String nazivCenovnika) {
		this.nazivCenovnika = nazivCenovnika;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idCenovnika != null ? idCenovnika.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cenovnik)) {
            return false;
        }
        Cenovnik other = (Cenovnik) object;
        if ((this.idCenovnika == null && other.idCenovnika != null) || (this.idCenovnika != null && !this.idCenovnika.equals(other.idCenovnika))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Cenovnik[ idCenovnika=" + idCenovnika + " ]";
    }
    
}
