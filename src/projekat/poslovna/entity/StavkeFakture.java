/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa
 */
@Entity
@Table(name = "stavke_fakture")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StavkeFakture.findAll", query = "SELECT s FROM StavkeFakture s"),
    @NamedQuery(name = "StavkeFakture.findByIdStavke", query = "SELECT s FROM StavkeFakture s WHERE s.idStavke = :idStavke"),
    @NamedQuery(name = "StavkeFakture.findByKolicina", query = "SELECT s FROM StavkeFakture s WHERE s.kolicina = :kolicina"),
    @NamedQuery(name = "StavkeFakture.findByRabat", query = "SELECT s FROM StavkeFakture s WHERE s.rabat = :rabat"),
    @NamedQuery(name = "StavkeFakture.findByJedinicnaCena", query = "SELECT s FROM StavkeFakture s WHERE s.jedinicnaCena = :jedinicnaCena"),
    @NamedQuery(name = "StavkeFakture.findByStopaPdvA", query = "SELECT s FROM StavkeFakture s WHERE s.stopaPdvA = :stopaPdvA"),
    @NamedQuery(name = "StavkeFakture.findByOsnovica", query = "SELECT s FROM StavkeFakture s WHERE s.osnovica = :osnovica"),
    @NamedQuery(name = "StavkeFakture.findByIznosPdvA", query = "SELECT s FROM StavkeFakture s WHERE s.iznosPdvA = :iznosPdvA"),
    @NamedQuery(name = "StavkeFakture.findByUkupanIznos", query = "SELECT s FROM StavkeFakture s WHERE s.ukupanIznos = :ukupanIznos")})
public class StavkeFakture implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STAVKE")
    private Integer idStavke;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "KOLICINA")
    private float kolicina;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "RABAT")
    private float rabat;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "JEDINICNA_CENA")
    private float jedinicnaCena;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "STOPA_PDV_A")
    private float stopaPdvA;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "OSNOVICA")
    private float osnovica;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IZNOS_PDV_A")
    private float iznosPdvA;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UKUPAN_IZNOS")
    private float ukupanIznos;
    
    @JoinColumn(name = "ID_FAKTURE", referencedColumnName = "ID_FAKTURE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Faktura idFakture;
    
    @JoinColumn(name = "ID_PROIZVODA", referencedColumnName = "ID_PROIZVODA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proizvod idProizvoda;

    public StavkeFakture() {
    }

  

    public StavkeFakture(float kolicina, float rabat, float jedinicnaCena, float stopaPdvA, float osnovica, float iznosPdvA, float ukupanIznos) {
        
        this.kolicina = kolicina;
        this.rabat = rabat;
        this.jedinicnaCena = jedinicnaCena;
        this.stopaPdvA = stopaPdvA;
        this.osnovica = osnovica;
        this.iznosPdvA = iznosPdvA;
        this.ukupanIznos = ukupanIznos;
    }

    public Integer getIdStavke() {
        return idStavke;
    }

    public void setIdStavke(Integer idStavke) {
        this.idStavke = idStavke;
    }

    public float getKolicina() {
        return kolicina;
    }

    public void setKolicina(float kolicina) {
        this.kolicina = kolicina;
    }

    public float getRabat() {
        return rabat;
    }

    public void setRabat(float rabat) {
        this.rabat = rabat;
    }

    public float getJedinicnaCena() {
        return jedinicnaCena;
    }

    public void setJedinicnaCena(float jedinicnaCena) {
        this.jedinicnaCena = jedinicnaCena;
    }

    public float getStopaPdvA() {
        return stopaPdvA;
    }

    public void setStopaPdvA(float stopaPdvA) {
        this.stopaPdvA = stopaPdvA;
    }

    public float getOsnovica() {
        return osnovica;
    }

    public void setOsnovica(float osnovica) {
        this.osnovica = osnovica;
    }

    public float getIznosPdvA() {
        return iznosPdvA;
    }

    public void setIznosPdvA(float iznosPdvA) {
        this.iznosPdvA = iznosPdvA;
    }

    public float getUkupanIznos() {
        return ukupanIznos;
    }

    public void setUkupanIznos(float ukupanIznos) {
        this.ukupanIznos = ukupanIznos;
    }

    public Faktura getIdFakture() {
        return idFakture;
    }

    public void setIdFakture(Faktura idFakture) {
        this.idFakture = idFakture;
    }

    public Proizvod getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(Proizvod idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStavke != null ? idStavke.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StavkeFakture)) {
            return false;
        }
        StavkeFakture other = (StavkeFakture) object;
        if ((this.idStavke == null && other.idStavke != null) || (this.idStavke != null && !this.idStavke.equals(other.idStavke))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StavkeFakture[ idStavke=" + idStavke + " ]";
    }
    
}
