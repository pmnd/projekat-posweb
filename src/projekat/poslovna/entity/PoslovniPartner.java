/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "poslovni_partner")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PoslovniPartner.findAll", query = "SELECT p FROM PoslovniPartner p"),
    @NamedQuery(name = "PoslovniPartner.findByIdPoslovnogPartnera", query = "SELECT p FROM PoslovniPartner p WHERE p.idPoslovnogPartnera = :idPoslovnogPartnera"),
    @NamedQuery(name = "PoslovniPartner.findByNaziv", query = "SELECT p FROM PoslovniPartner p WHERE p.naziv = :naziv"),
    @NamedQuery(name = "PoslovniPartner.findByAdresa", query = "SELECT p FROM PoslovniPartner p WHERE p.adresa = :adresa"),
    @NamedQuery(name = "PoslovniPartner.findByPib", query = "SELECT p FROM PoslovniPartner p WHERE p.pib = :pib"),
    @NamedQuery(name = "PoslovniPartner.findByVrsta", query = "SELECT p FROM PoslovniPartner p WHERE p.vrsta = :vrsta")})
public class PoslovniPartner implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_POSLOVNOG_PARTNERA")
    private Integer idPoslovnogPartnera;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "NAZIV")
    private String naziv;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "ADRESA")
    private String adresa;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "PIB")
    private String pib;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "VRSTA")
    private String vrsta;
    
    @JoinColumn(name = "ID_PREDUZECA", referencedColumnName = "ID_PREDUZECA")
    @ManyToOne(fetch = FetchType.EAGER)
    private Preduzece idPreduzeca;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoslovnogPartnera", fetch = FetchType.EAGER)
    private Set<Faktura> fakturaSet;
    
    
    
    
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoslovnogPartnera", fetch = FetchType.EAGER)
    private Set<Narudzbenica> narudzbenicaSet;

    public Set<Narudzbenica> getNarudzbenicaSet() {
		return narudzbenicaSet;
	}


	public void setNarudzbenicaSet(Set<Narudzbenica> narudzbenicaSet) {
		this.narudzbenicaSet = narudzbenicaSet;
	}

	
	
	
	
	
	
	
	

	public PoslovniPartner() {
    }


    public PoslovniPartner(String naziv, String adresa, String pib, int idPoslovnogPartnera, String vrsta) {
        
        this.naziv = naziv;
        this.adresa = adresa;
        this.pib = pib;
        this.idPoslovnogPartnera = idPoslovnogPartnera;
        this.vrsta = vrsta;
    }
    
    public Integer getIdPoslovnogPartnera() {
        return idPoslovnogPartnera;
    }

    public void setIdPoslovnogPartnera(Integer idPoslovnogPartnera) {
        this.idPoslovnogPartnera = idPoslovnogPartnera;
    }

	public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }
    
    public Preduzece getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Preduzece idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }
    
    @XmlTransient
    public Set<Faktura> getFakturaSet() {
        return fakturaSet;
    }

    public void setFakturaSet(Set<Faktura> fakturaSet) {
        this.fakturaSet = fakturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPoslovnogPartnera != null ? idPoslovnogPartnera.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoslovniPartner)) {
            return false;
        }
        PoslovniPartner other = (PoslovniPartner) object;
        if ((this.idPoslovnogPartnera == null && other.idPoslovnogPartnera != null) || (this.idPoslovnogPartnera != null && !this.idPoslovnogPartnera.equals(other.idPoslovnogPartnera))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PoslovniPartner[ idPoslovnogPartnera=" + idPoslovnogPartnera + " ]";
    }
    
}
