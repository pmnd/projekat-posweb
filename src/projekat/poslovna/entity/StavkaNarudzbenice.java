/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "stavka_narudzbenice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StavkaNarudzbenice.findAll", query = "SELECT s FROM StavkaNarudzbenice s"),
    @NamedQuery(name = "StavkaNarudzbenice.findByIdStavkeNarudzbenice", query = "SELECT s FROM StavkaNarudzbenice s WHERE s.idStavkeNarudzbenice = :idStavkeNarudzbenice"),
    @NamedQuery(name = "StavkaNarudzbenice.findByKolicinaStavkeNar", query = "SELECT s FROM StavkaNarudzbenice s WHERE s.kolicinaStavkeNar = :kolicinaStavkeNar"),
    @NamedQuery(name = "StavkaNarudzbenice.findByJedinicnaCenaStavkeNar", query = "SELECT s FROM StavkaNarudzbenice s WHERE s.jedinicnaCenaStavkeNar = :jedinicnaCenaStavkeNar")})
public class StavkaNarudzbenice implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STAVKE_NARUDZBENICE")
    private Integer idStavkeNarudzbenice;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "KOLICINA_STAVKE_NAR")
    private float kolicinaStavkeNar;
    
    @Column(name = "JEDINICNA_CENA_STAVKE_NAR")
    private float jedinicnaCenaStavkeNar;
    
    @JoinColumn(name = "ID_NARUDZBENICE", referencedColumnName = "ID_NARUDZBENICE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Narudzbenica idNarudzbenice;
    
    @JoinColumn(name = "ID_PROIZVODA", referencedColumnName = "ID_PROIZVODA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proizvod idProizvoda;

    public StavkaNarudzbenice() {
    }

    public StavkaNarudzbenice(float kolicinaStavkeNar, float jedinicnaCenaStavke) {
        this.kolicinaStavkeNar = kolicinaStavkeNar;
        this.jedinicnaCenaStavkeNar = jedinicnaCenaStavke;
    }

    public Integer getIdStavkeNarudzbenice() {
        return idStavkeNarudzbenice;
    }

    public void setIdStavkeNarudzbenice(Integer idStavkeNarudzbenice) {
        this.idStavkeNarudzbenice = idStavkeNarudzbenice;
    }

    public float getKolicinaStavkeNar() {
        return kolicinaStavkeNar;
    }

    public void setKolicinaStavkeNar(float kolicinaStavkeNar) {
        this.kolicinaStavkeNar = kolicinaStavkeNar;
    }

    public float getJedinicnaCenaStavkeNar() {
        return jedinicnaCenaStavkeNar;
    }

    public void setJedinicnaCenaStavkeNar(float jedinicnaCenaStavkeNar) {
        this.jedinicnaCenaStavkeNar = jedinicnaCenaStavkeNar;
    }

    public Narudzbenica getIdNarudzbenice() {
        return idNarudzbenice;
    }

    public void setIdNarudzbenice(Narudzbenica idNarudzbenice) {
        this.idNarudzbenice = idNarudzbenice;
    }

    public Proizvod getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(Proizvod idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStavkeNarudzbenice != null ? idStavkeNarudzbenice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StavkaNarudzbenice)) {
            return false;
        }
        StavkaNarudzbenice other = (StavkaNarudzbenice) object;
        if ((this.idStavkeNarudzbenice == null && other.idStavkeNarudzbenice != null) || (this.idStavkeNarudzbenice != null && !this.idStavkeNarudzbenice.equals(other.idStavkeNarudzbenice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StavkaNarudzbenice[ idStavkeNarudzbenice=" + idStavkeNarudzbenice + " ]";
    }
    
}
