/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "porez")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Porez.findAll", query = "SELECT p FROM Porez p"),
    @NamedQuery(name = "Porez.findByIdPoreza", query = "SELECT p FROM Porez p WHERE p.idPoreza = :idPoreza"),
    @NamedQuery(name = "Porez.findByNazivPoreza", query = "SELECT p FROM Porez p WHERE p.nazivPoreza = :nazivPoreza")})
public class Porez implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_POREZA")
    private Integer idPoreza;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "NAZIV_POREZA")
    private String nazivPoreza;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoreza", fetch = FetchType.EAGER)
    private Set<StopaPoreza> stopaPorezaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoreza", fetch = FetchType.EAGER)
    private Set<GrupaProizvoda> grupaProizvodaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoreza", fetch = FetchType.EAGER)
    private Set<ObracunatiPorezi> obracunatiPoreziSet;

    public Porez() {
    }

   

    public Porez(String nazivPoreza) {
        this.nazivPoreza = nazivPoreza;
    }

    public Integer getIdPoreza() {
        return idPoreza;
    }

    public void setIdPoreza(Integer idPoreza) {
        this.idPoreza = idPoreza;
    }

    public String getNazivPoreza() {
        return nazivPoreza;
    }

    public void setNazivPoreza(String nazivPoreza) {
        this.nazivPoreza = nazivPoreza;
    }

    @XmlTransient
    public Set<StopaPoreza> getStopaPorezaSet() {
        return stopaPorezaSet;
    }

    public void setStopaPorezaSet(Set<StopaPoreza> stopaPorezaSet) {
        this.stopaPorezaSet = stopaPorezaSet;
    }

    @XmlTransient
    public Set<GrupaProizvoda> getGrupaProizvodaSet() {
        return grupaProizvodaSet;
    }

    public void setGrupaProizvodaSet(Set<GrupaProizvoda> grupaProizvodaSet) {
        this.grupaProizvodaSet = grupaProizvodaSet;
    }

    @XmlTransient
    public Set<ObracunatiPorezi> getObracunatiPoreziSet() {
        return obracunatiPoreziSet;
    }

    public void setObracunatiPoreziSet(Set<ObracunatiPorezi> obracunatiPoreziSet) {
        this.obracunatiPoreziSet = obracunatiPoreziSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPoreza != null ? idPoreza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Porez)) {
            return false;
        }
        Porez other = (Porez) object;
        if ((this.idPoreza == null && other.idPoreza != null) || (this.idPoreza != null && !this.idPoreza.equals(other.idPoreza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Porez[ idPoreza=" + idPoreza + " ]";
    }
    
}