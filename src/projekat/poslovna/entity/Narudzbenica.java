/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "narudzbenica")
@XmlRootElement

@NamedQueries({
    @NamedQuery(name = "Narudzbenica.findAll", query = "SELECT n FROM Narudzbenica n"),
    @NamedQuery(name = "Narudzbenica.findByIdNarudzbenice", query = "SELECT n FROM Narudzbenica n WHERE n.idNarudzbenice = :idNarudzbenice"),
    @NamedQuery(name = "Narudzbenica.findByBrojNarudzbenice", query = "SELECT n FROM Narudzbenica n WHERE n.brojNarudzbenice = :brojNarudzbenice"),
    @NamedQuery(name = "Narudzbenica.findByDatum", query = "SELECT n FROM Narudzbenica n WHERE n.datum = :datum"),
    @NamedQuery(name = "Narudzbenica.findByPrihvacena", query = "SELECT n FROM Narudzbenica n WHERE n.prihvacena = :prihvacena"),
    @NamedQuery(name = "Narudzbenica.createFakturaSP", query = "SELECT n FROM Narudzbenica n WHERE n.datum = :datum")})

@NamedNativeQuery(name = "kreiranjeFaktureOdNarudzbine", query = "{call KreiranjeFaktureOdNarudzbine(:param)}")

public class Narudzbenica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_NARUDZBENICE")
    private Integer idNarudzbenice;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "BROJ_NARUDZBENICE")
    private int brojNarudzbenice;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_")
    @Temporal(TemporalType.DATE)
    private Date datum;
    
    @Column(name = "PRIHVACENA")
    private int prihvacena;
    
   
    @JoinColumn(name = "ID_GODINE", referencedColumnName = "ID_GODINE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private PoslovnaGodina idGodine;
    
    @JoinColumn(name = "ID_POSLOVNOG_PARTNERA", referencedColumnName = "ID_POSLOVNOG_PARTNERA")
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private PoslovniPartner idPoslovnogPartnera;
	
	@OneToMany(mappedBy = "idNarudzbenice", fetch = FetchType.EAGER)
    private Set<Faktura> fakturaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNarudzbenice", fetch = FetchType.EAGER)
    private Set<StavkaNarudzbenice> stavkaNarudzbeniceSet;

    public Narudzbenica() {
    }

    public Narudzbenica(int brojNarudzbenice, Date datum, int prihvacena) {
        
        this.brojNarudzbenice = brojNarudzbenice;
        this.datum = datum;
        this.prihvacena = prihvacena;
    }

    public Integer getIdNarudzbenice() {
        return idNarudzbenice;
    }

    public void setIdNarudzbenice(Integer idNarudzbenice) {
        this.idNarudzbenice = idNarudzbenice;
    }

    public int getBrojNarudzbenice() {
        return brojNarudzbenice;
    }
    
    public PoslovniPartner getIdPoslovnogPartnera() {
		return idPoslovnogPartnera;
	}


	public void setIdPoslovnogPartnera(PoslovniPartner idPoslovnogPartnera) {
		this.idPoslovnogPartnera = idPoslovnogPartnera;
	}

    public void setBrojNarudzbenice(int brojNarudzbenice) {
        this.brojNarudzbenice = brojNarudzbenice;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getPrihvacena() {
        return prihvacena;
    }

    public void setPrihvacena(int prihvacena) {
        this.prihvacena = prihvacena;
    }

    public PoslovnaGodina getIdGodine() {
        return idGodine;
    }

    public void setIdGodine(PoslovnaGodina idGodine) {
        this.idGodine = idGodine;
    }

    @XmlTransient
    public Set<Faktura> getFakturaSet() {
        return fakturaSet;
    }

    public void setFakturaSet(Set<Faktura> fakturaSet) {
        this.fakturaSet = fakturaSet;
    }

    @XmlTransient
    public Set<StavkaNarudzbenice> getStavkaNarudzbeniceSet() {
        return stavkaNarudzbeniceSet;
    }

    public void setStavkaNarudzbeniceSet(Set<StavkaNarudzbenice> stavkaNarudzbeniceSet) {
        this.stavkaNarudzbeniceSet = stavkaNarudzbeniceSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNarudzbenice != null ? idNarudzbenice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Narudzbenica)) {
            return false;
        }
        Narudzbenica other = (Narudzbenica) object;
        if ((this.idNarudzbenice == null && other.idNarudzbenice != null) || (this.idNarudzbenice != null && !this.idNarudzbenice.equals(other.idNarudzbenice))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Narudzbenica[ idNarudzbenice=" + idNarudzbenice + " ]";
    }
    
}
