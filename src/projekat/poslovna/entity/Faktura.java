/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "faktura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Faktura.findAll", query = "SELECT f FROM Faktura f"),
    @NamedQuery(name = "Faktura.findByIdFakture", query = "SELECT f FROM Faktura f WHERE f.idFakture = :idFakture"),
    @NamedQuery(name = "Faktura.findByBrojFakture", query = "SELECT f FROM Faktura f WHERE f.brojFakture = :brojFakture"),
    @NamedQuery(name = "Faktura.findByDatumFakture", query = "SELECT f FROM Faktura f WHERE f.datumFakture = :datumFakture"),
    @NamedQuery(name = "Faktura.findByDatumPlacanjaFakture", query = "SELECT f FROM Faktura f WHERE f.datumPlacanjaFakture = :datumPlacanjaFakture"),
    @NamedQuery(name = "Faktura.findByUkupanRabat", query = "SELECT f FROM Faktura f WHERE f.ukupanRabat = :ukupanRabat"),
    @NamedQuery(name = "Faktura.findByUkupanIznosBezPdvA", query = "SELECT f FROM Faktura f WHERE f.ukupanIznosBezPdvA = :ukupanIznosBezPdvA"),
    @NamedQuery(name = "Faktura.findByUkupanPdv", query = "SELECT f FROM Faktura f WHERE f.ukupanPdv = :ukupanPdv"),
    @NamedQuery(name = "Faktura.findByUkupnoZaPlacanje", query = "SELECT f FROM Faktura f WHERE f.ukupnoZaPlacanje = :ukupnoZaPlacanje"),
    @NamedQuery(name = "Faktura.findByStatus", query = "SELECT f FROM Faktura f WHERE f.status = :status")})
public class Faktura implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FAKTURE")
    private Integer idFakture;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "BROJ_FAKTURE")
    private int brojFakture;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_FAKTURE")
    @Temporal(TemporalType.DATE)
    private Date datumFakture;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_PLACANJA_FAKTURE")
    @Temporal(TemporalType.DATE)
    private Date datumPlacanjaFakture;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UKUPAN_RABAT")
    private float ukupanRabat;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UKUPAN_IZNOS_BEZ_PDV_A")
    private float ukupanIznosBezPdvA;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UKUPAN_PDV")
    private float ukupanPdv;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UKUPNO_ZA_PLACANJE")
    private float ukupnoZaPlacanje;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private int status;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFakture", fetch = FetchType.EAGER)
    private Set<StavkeFakture> stavkeFaktureSet;
      
    @JoinColumn(name = "ID_POSLOVNOG_PARTNERA", referencedColumnName = "ID_POSLOVNOG_PARTNERA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private PoslovniPartner idPoslovnogPartnera;
    
    @JoinColumn(name = "ID_RACUNA", referencedColumnName = "ID_RACUNA")
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private RacunUBanci idRacunaUBanci;
    
	@JoinColumn(name = "ID_NARUDZBENICE", referencedColumnName = "ID_NARUDZBENICE")
    @ManyToOne(fetch = FetchType.EAGER)
    private Narudzbenica idNarudzbenice;
    
    
    @JoinColumn(name = "ID_GODINE", referencedColumnName = "ID_GODINE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private PoslovnaGodina idGodine;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFakture", fetch = FetchType.EAGER)
    private Set<ObracunatiPorezi> obracunatiPoreziSet;

    public Faktura() {
    }


    public Faktura(int brojFakture, Date datumFakture, Date datumPlacanjaFakture, float ukupanRabat, float ukupanIznosBezPdvA, float ukupanPdv, float ukupnoZaPlacanje, int status) {
        
        this.brojFakture = brojFakture;
        this.datumFakture = datumFakture;
        this.datumPlacanjaFakture = datumPlacanjaFakture;
        this.ukupanRabat = ukupanRabat;
        this.ukupanIznosBezPdvA = ukupanIznosBezPdvA;
        this.ukupanPdv = ukupanPdv;
        this.ukupnoZaPlacanje = ukupnoZaPlacanje;
        this.status = status;
    }
    
    public Integer getIdFakture() {
        return idFakture;
    }

    public void setIdFakture(Integer idFakture) {
        this.idFakture = idFakture;
    }

    public int getBrojFakture() {
        return brojFakture;
    }
    
    public RacunUBanci getIdRacunaUBanci() {
		return idRacunaUBanci;
	}


	public void setIdRacunaUBanci(RacunUBanci idRacunaUBanci) {
		this.idRacunaUBanci = idRacunaUBanci;
	}

    public void setBrojFakture(int brojFakture) {
        this.brojFakture = brojFakture;
    }

    public Date getDatumFakture() {
        return datumFakture;
    }

    public void setDatumFakture(Date datumFakture) {
        this.datumFakture = datumFakture;
    }

    public Date getDatumPlacanjaFakture() {
        return datumPlacanjaFakture;
    }

    public void setDatumPlacanjaFakture(Date datumPlacanjaFakture) {
        this.datumPlacanjaFakture = datumPlacanjaFakture;
    }

    public float getUkupanRabat() {
        return ukupanRabat;
    }

    public void setUkupanRabat(float ukupanRabat) {
        this.ukupanRabat = ukupanRabat;
    }

    public float getUkupanIznosBezPdvA() {
        return ukupanIznosBezPdvA;
    }

    public void setUkupanIznosBezPdvA(float ukupanIznosBezPdvA) {
        this.ukupanIznosBezPdvA = ukupanIznosBezPdvA;
    }

    public float getUkupanPdv() {
        return ukupanPdv;
    }

    public void setUkupanPdv(float ukupanPdv) {
        this.ukupanPdv = ukupanPdv;
    }

    public float getUkupnoZaPlacanje() {
        return ukupnoZaPlacanje;
    }

    public void setUkupnoZaPlacanje(float ukupnoZaPlacanje) {
        this.ukupnoZaPlacanje = ukupnoZaPlacanje;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @XmlTransient
    public Set<StavkeFakture> getStavkeFaktureSet() {
        return stavkeFaktureSet;
    }

    public void setStavkeFaktureSet(Set<StavkeFakture> stavkeFaktureSet) {
        this.stavkeFaktureSet = stavkeFaktureSet;
    }

    public PoslovniPartner getIdPoslovnogPartnera() {
        return idPoslovnogPartnera;
    }

    public void setIdPoslovnogPartnera(PoslovniPartner idPoslovnogPartnera) {
        this.idPoslovnogPartnera = idPoslovnogPartnera;
    }

    public Narudzbenica getIdNarudzbenice() {
        return idNarudzbenice;
    }

    public void setIdNarudzbenice(Narudzbenica idNarudzbenice) {
        this.idNarudzbenice = idNarudzbenice;
    }

    public PoslovnaGodina getIdGodine() {
        return idGodine;
    }

    public void setIdGodine(PoslovnaGodina idGodine) {
        this.idGodine = idGodine;
    }

    @XmlTransient
    public Set<ObracunatiPorezi> getObracunatiPoreziSet() {
        return obracunatiPoreziSet;
    }

    public void setObracunatiPoreziSet(Set<ObracunatiPorezi> obracunatiPoreziSet) {
        this.obracunatiPoreziSet = obracunatiPoreziSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFakture != null ? idFakture.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faktura)) {
            return false;
        }
        Faktura other = (Faktura) object;
        if ((this.idFakture == null && other.idFakture != null) || (this.idFakture != null && !this.idFakture.equals(other.idFakture))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Faktura[ idFakture=" + idFakture + " ]";
    }
    
}
