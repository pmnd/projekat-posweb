/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "proizvod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proizvod.findAll", query = "SELECT p FROM Proizvod p"),
    @NamedQuery(name = "Proizvod.findByIdProizvoda", query = "SELECT p FROM Proizvod p WHERE p.idProizvoda = :idProizvoda"),
    @NamedQuery(name = "Proizvod.findByNazivProizvoda", query = "SELECT p FROM Proizvod p WHERE p.nazivProizvoda = :nazivProizvoda")})
public class Proizvod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PROIZVODA")
    private Integer idProizvoda;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "NAZIV_PROIZVODA")
    private String nazivProizvoda;
    
    @Column(name = "JEDINICNA_KOLICINA")
    private float jedinicnaKolicina;
    
   
	@JoinColumn(name = "ID_GRUPE", referencedColumnName = "ID_GRUPE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private GrupaProizvoda idGrupe;
    
    @JoinColumn(name = "ID_JEDINICE", referencedColumnName = "ID_JEDINICE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private JedinicaMere idJedinice;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProizvoda", fetch = FetchType.EAGER)
    private Set<StavkeFakture> stavkeFaktureSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProizvoda", fetch = FetchType.EAGER)
    private Set<StavkeCenovnika> stavkeCenovnikaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProizvoda", fetch = FetchType.EAGER)
    private Set<StavkaNarudzbenice> stavkaNarudzbeniceSet;

    public Proizvod() {
    }

 

    

    public Proizvod(String nazivProizvoda, float jedinicnaKolicina) {
		super();
		this.nazivProizvoda = nazivProizvoda;
		this.jedinicnaKolicina = jedinicnaKolicina;
		
	}





	public Integer getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(Integer idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    public String getNazivProizvoda() {
        return nazivProizvoda;
    }

    public void setNazivProizvoda(String nazivProizvoda) {
        this.nazivProizvoda = nazivProizvoda;
    }

    public GrupaProizvoda getIdGrupe() {
        return idGrupe;
    }

    public void setIdGrupe(GrupaProizvoda idGrupe) {
        this.idGrupe = idGrupe;
    }

    public JedinicaMere getIdJedinice() {
        return idJedinice;
    }
    
    public float getJedinicnaKolicina() {
		return jedinicnaKolicina;
	}



	public void setJedinicnaKolicina(float jedinicnaKolicina) {
		this.jedinicnaKolicina = jedinicnaKolicina;
	}


    public void setIdJedinice(JedinicaMere idJedinice) {
        this.idJedinice = idJedinice;
    }

    @XmlTransient
    public Set<StavkeFakture> getStavkeFaktureSet() {
        return stavkeFaktureSet;
    }

    public void setStavkeFaktureSet(Set<StavkeFakture> stavkeFaktureSet) {
        this.stavkeFaktureSet = stavkeFaktureSet;
    }

    
    @XmlTransient
    public Set<StavkeCenovnika> getStavkeCenovnikaSet() {
        return stavkeCenovnikaSet;
    }

    public void setStavkeCenovnikaSet(Set<StavkeCenovnika> stavkeCenovnikaSet) {
        this.stavkeCenovnikaSet = stavkeCenovnikaSet;
    }

    @XmlTransient
    public Set<StavkaNarudzbenice> getStavkaNarudzbeniceSet() {
        return stavkaNarudzbeniceSet;
    }

    public void setStavkaNarudzbeniceSet(Set<StavkaNarudzbenice> stavkaNarudzbeniceSet) {
        this.stavkaNarudzbeniceSet = stavkaNarudzbeniceSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProizvoda != null ? idProizvoda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proizvod)) {
            return false;
        }
        Proizvod other = (Proizvod) object;
        if ((this.idProizvoda == null && other.idProizvoda != null) || (this.idProizvoda != null && !this.idProizvoda.equals(other.idProizvoda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Proizvod[ idProizvoda=" + idProizvoda + " ]";
    }
    
}
