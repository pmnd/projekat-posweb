/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "stavke_cenovnika")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StavkeCenovnika.findAll", query = "SELECT s FROM StavkeCenovnika s"),
    @NamedQuery(name = "StavkeCenovnika.findByCena", query = "SELECT s FROM StavkeCenovnika s WHERE s.cena = :cena"),
    @NamedQuery(name = "StavkeCenovnika.findByIdStavkeCenovnika", query = "SELECT s FROM StavkeCenovnika s WHERE s.idStavkeCenovnika = :idStavkeCenovnika")})
public class StavkeCenovnika implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CENA")
    private float cena;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STAVKE_CENOVNIKA")
    private Integer idStavkeCenovnika;
    
    @JoinColumn(name = "ID_PROIZVODA", referencedColumnName = "ID_PROIZVODA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proizvod idProizvoda;
    
    @JoinColumn(name = "ID_CENOVNIKA", referencedColumnName = "ID_CENOVNIKA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Cenovnik idCenovnika;

    public StavkeCenovnika() {
    }

 
    public StavkeCenovnika(float cena) {
        this.cena = cena;
    }

    public float getCena() {
        return cena;
    }

    public void setCena(float cena) {
        this.cena = cena;
    }

    public Integer getIdStavkeCenovnika() {
        return idStavkeCenovnika;
    }

    public void setIdStavkeCenovnika(Integer idStavkeCenovnika) {
        this.idStavkeCenovnika = idStavkeCenovnika;
    }

    public Proizvod getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(Proizvod idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    public Cenovnik getIdCenovnika() {
        return idCenovnika;
    }

    public void setIdCenovnika(Cenovnik idCenovnika) {
        this.idCenovnika = idCenovnika;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStavkeCenovnika != null ? idStavkeCenovnika.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StavkeCenovnika)) {
            return false;
        }
        StavkeCenovnika other = (StavkeCenovnika) object;
        if ((this.idStavkeCenovnika == null && other.idStavkeCenovnika != null) || (this.idStavkeCenovnika != null && !this.idStavkeCenovnika.equals(other.idStavkeCenovnika))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StavkeCenovnika[ idStavkeCenovnika=" + idStavkeCenovnika + " ]";
    }
    
}
