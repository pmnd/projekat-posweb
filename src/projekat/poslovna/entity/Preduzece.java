/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "preduzece")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preduzece.findAll", query = "SELECT p FROM Preduzece p"),
    @NamedQuery(name = "Preduzece.findByIdPreduzeca", query = "SELECT p FROM Preduzece p WHERE p.idPreduzeca = :idPreduzeca"),
    @NamedQuery(name = "Preduzece.findByNazivPreduzeca", query = "SELECT p FROM Preduzece p WHERE p.nazivPreduzeca = :nazivPreduzeca"),
    @NamedQuery(name = "Preduzece.findByAdresa", query = "SELECT p FROM Preduzece p WHERE p.adresa = :adresa"),
    @NamedQuery(name = "Preduzece.findByPib", query = "SELECT p FROM Preduzece p WHERE p.pib = :pib")})
public class Preduzece implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PREDUZECA")
    private Integer idPreduzeca;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "NAZIV_PREDUZECA")
    private String nazivPreduzeca;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "ADRESA_")
    private String adresa;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "PIB")
    private String pib;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPreduzeca", fetch = FetchType.EAGER)
    private Set<RacunUBanci> racunUBanciSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPreduzeca", fetch = FetchType.EAGER)
    private Set<Cenovnik> cenovnikSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPreduzeca", fetch = FetchType.EAGER)
    private Set<GrupaProizvoda> grupaProizvodaSet;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPreduzeca", fetch = FetchType.EAGER)
    private Set<PoslovnaGodina> poslovnaGodinaSet;
    
    @OneToMany(mappedBy = "idPreduzeca", fetch = FetchType.EAGER)
    private Set<PoslovniPartner> poslovniPartnerSet;
    
    public Preduzece() {
    }

    public Preduzece(String nazivPreduzeca, String adresa, String pib) {
       
        this.nazivPreduzeca = nazivPreduzeca;
        this.adresa = adresa;
        this.pib = pib;
    }

    public Integer getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Integer idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }

    public String getNazivPreduzeca() {
        return nazivPreduzeca;
    }

    public void setNazivPreduzeca(String nazivPreduzeca) {
        this.nazivPreduzeca = nazivPreduzeca;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    @XmlTransient
    public Set<RacunUBanci> getRacunUBanciSet() {
        return racunUBanciSet;
    }

    public void setRacunUBanciSet(Set<RacunUBanci> racunUBanciSet) {
        this.racunUBanciSet = racunUBanciSet;
    }

    @XmlTransient
    public Set<Cenovnik> getCenovnikSet() {
        return cenovnikSet;
    }

    public void setCenovnikSet(Set<Cenovnik> cenovnikSet) {
        this.cenovnikSet = cenovnikSet;
    }

    @XmlTransient
    public Set<GrupaProizvoda> getGrupaProizvodaSet() {
        return grupaProizvodaSet;
    }

    public void setGrupaProizvodaSet(Set<GrupaProizvoda> grupaProizvodaSet) {
        this.grupaProizvodaSet = grupaProizvodaSet;
    }

    @XmlTransient
    public Set<PoslovnaGodina> getPoslovnaGodinaSet() {
        return poslovnaGodinaSet;
    }

    public void setPoslovnaGodinaSet(Set<PoslovnaGodina> poslovnaGodinaSet) {
        this.poslovnaGodinaSet = poslovnaGodinaSet;
    }

    @XmlTransient
    public Set<PoslovniPartner> getPoslovniPartnerSet() {
        return poslovniPartnerSet;
    }

    public void setPoslovniPartnerSet(Set<PoslovniPartner> poslovniPartnerSet) {
        this.poslovniPartnerSet = poslovniPartnerSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPreduzeca != null ? idPreduzeca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preduzece)) {
            return false;
        }
        Preduzece other = (Preduzece) object;
        if ((this.idPreduzeca == null && other.idPreduzeca != null) || (this.idPreduzeca != null && !this.idPreduzeca.equals(other.idPreduzeca))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Preduzece[ idPreduzeca=" + idPreduzeca + " ]";
    }
    
}
