/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "grupa_proizvoda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupaProizvoda.findAll", query = "SELECT g FROM GrupaProizvoda g"),
    @NamedQuery(name = "GrupaProizvoda.findByIdGrupe", query = "SELECT g FROM GrupaProizvoda g WHERE g.idGrupe = :idGrupe"),
    @NamedQuery(name = "GrupaProizvoda.findByNazivGrupe", query = "SELECT g FROM GrupaProizvoda g WHERE g.nazivGrupe = :nazivGrupe")})
public class GrupaProizvoda implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_GRUPE")
    private Integer idGrupe;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "NAZIV_GRUPE")
    private String nazivGrupe;
    
    @JoinColumn(name = "ID_PREDUZECA", referencedColumnName = "ID_PREDUZECA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Preduzece idPreduzeca;
    
    @JoinColumn(name = "ID_POREZA", referencedColumnName = "ID_POREZA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Porez idPoreza;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGrupe", fetch = FetchType.EAGER)
    private Set<Proizvod> proizvodSet;

    public GrupaProizvoda() {
    }


    public GrupaProizvoda(String nazivGrupe) {
        this.nazivGrupe = nazivGrupe;
    }

    public Integer getIdGrupe() {
        return idGrupe;
    }

    public void setIdGrupe(Integer idGrupe) {
        this.idGrupe = idGrupe;
    }

    public String getNazivGrupe() {
        return nazivGrupe;
    }

    public void setNazivGrupe(String nazivGrupe) {
        this.nazivGrupe = nazivGrupe;
    }

    public Preduzece getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Preduzece idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }

    public Porez getIdPoreza() {
        return idPoreza;
    }

    public void setIdPoreza(Porez idPoreza) {
        this.idPoreza = idPoreza;
    }

    @XmlTransient
    public Set<Proizvod> getProizvodSet() {
        return proizvodSet;
    }

    public void setProizvodSet(Set<Proizvod> proizvodSet) {
        this.proizvodSet = proizvodSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupe != null ? idGrupe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupaProizvoda)) {
            return false;
        }
        GrupaProizvoda other = (GrupaProizvoda) object;
        if ((this.idGrupe == null && other.idGrupe != null) || (this.idGrupe != null && !this.idGrupe.equals(other.idGrupe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.GrupaProizvoda[ idGrupe=" + idGrupe + " ]";
    }
    
}
