/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "poslovna_godina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PoslovnaGodina.findAll", query = "SELECT p FROM PoslovnaGodina p"),
    @NamedQuery(name = "PoslovnaGodina.findByIdGodine", query = "SELECT p FROM PoslovnaGodina p WHERE p.idGodine = :idGodine"),
    @NamedQuery(name = "PoslovnaGodina.findByGodina", query = "SELECT p FROM PoslovnaGodina p WHERE p.godina = :godina"),
    @NamedQuery(name = "PoslovnaGodina.findByDaLiJeZakljucena", query = "SELECT p FROM PoslovnaGodina p WHERE p.daLiJeZakljucena = :daLiJeZakljucena")})
public class PoslovnaGodina implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_GODINE")
    private Integer idGodine;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "GODINA")
    private int godina;
    
    @Column(name = "DA_LI_JE_ZAKLJUCENA")
    private Short daLiJeZakljucena;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGodine", fetch = FetchType.EAGER)
    private Set<Narudzbenica> narudzbenicaSet;
    
    @JoinColumn(name = "ID_PREDUZECA", referencedColumnName = "ID_PREDUZECA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Preduzece idPreduzeca;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGodine", fetch = FetchType.EAGER)
    private Set<Faktura> fakturaSet;

    public PoslovnaGodina() {
    }

 
    public PoslovnaGodina(int godina) {
        this.godina = godina;
    }

    public Integer getIdGodine() {
        return idGodine;
    }

    public void setIdGodine(Integer idGodine) {
        this.idGodine = idGodine;
    }

    public int getGodina() {
        return godina;
    }

    public void setGodina(int godina) {
        this.godina = godina;
    }

    public Short getDaLiJeZakljucena() {
        return daLiJeZakljucena;
    }

    public void setDaLiJeZakljucena(Short daLiJeZakljucena) {
        this.daLiJeZakljucena = daLiJeZakljucena;
    }

    @XmlTransient
    public Set<Narudzbenica> getNarudzbenicaSet() {
        return narudzbenicaSet;
    }

    public void setNarudzbenicaSet(Set<Narudzbenica> narudzbenicaSet) {
        this.narudzbenicaSet = narudzbenicaSet;
    }

    public Preduzece getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Preduzece idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }

    @XmlTransient
    public Set<Faktura> getFakturaSet() {
        return fakturaSet;
    }

    public void setFakturaSet(Set<Faktura> fakturaSet) {
        this.fakturaSet = fakturaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGodine != null ? idGodine.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoslovnaGodina)) {
            return false;
        }
        PoslovnaGodina other = (PoslovnaGodina) object;
        if ((this.idGodine == null && other.idGodine != null) || (this.idGodine != null && !this.idGodine.equals(other.idGodine))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PoslovnaGodina[ idGodine=" + idGodine + " ]";
    }
    
}
