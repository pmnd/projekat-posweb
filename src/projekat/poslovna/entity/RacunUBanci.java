/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "racun_u_banci")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RacunUBanci.findAll", query = "SELECT r FROM RacunUBanci r"),
    @NamedQuery(name = "RacunUBanci.findByIdRacuna", query = "SELECT r FROM RacunUBanci r WHERE r.idRacuna = :idRacuna"),
    @NamedQuery(name = "RacunUBanci.findByBrojRacuna", query = "SELECT r FROM RacunUBanci r WHERE r.brojRacuna = :brojRacuna"),
    @NamedQuery(name = "RacunUBanci.findByNazivBanke", query = "SELECT r FROM RacunUBanci r WHERE r.nazivBanke = :nazivBanke")})
public class RacunUBanci implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RACUNA")
    private Integer idRacuna;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "BROJ_RACUNA")
    private String brojRacuna;
    
    @Size(max = 64)
    @Column(name = "NAZIV_BANKE")
    private String nazivBanke;
    
    @JoinColumn(name = "ID_PREDUZECA", referencedColumnName = "ID_PREDUZECA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Preduzece idPreduzeca;
    
    
    
    
    
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRacunaUBanci", fetch = FetchType.EAGER)
    private Set<Faktura> fakture;

    public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}

	
	
	
	
	
	public RacunUBanci() {
    }

    public RacunUBanci(String brojRacuna, String nazivBanke) {
        this.nazivBanke = nazivBanke;
        this.brojRacuna = brojRacuna;
    }

    public Integer getIdRacuna() {
        return idRacuna;
    }

    public void setIdRacuna(Integer idRacuna) {
        this.idRacuna = idRacuna;
    }

    public String getBrojRacuna() {
        return brojRacuna;
    }

    public void setBrojRacuna(String brojRacuna) {
        this.brojRacuna = brojRacuna;
    }

    public String getNazivBanke() {
        return nazivBanke;
    }

    public void setNazivBanke(String nazivBanke) {
        this.nazivBanke = nazivBanke;
    }

    public Preduzece getIdPreduzeca() {
        return idPreduzeca;
    }

    public void setIdPreduzeca(Preduzece idPreduzeca) {
        this.idPreduzeca = idPreduzeca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRacuna != null ? idRacuna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RacunUBanci)) {
            return false;
        }
        RacunUBanci other = (RacunUBanci) object;
        if ((this.idRacuna == null && other.idRacuna != null) || (this.idRacuna != null && !this.idRacuna.equals(other.idRacuna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RacunUBanci[ idRacuna=" + idRacuna + " ]";
    }
    
}
