/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "stopa_poreza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StopaPoreza.findAll", query = "SELECT s FROM StopaPoreza s"),
    @NamedQuery(name = "StopaPoreza.findByIdStope", query = "SELECT s FROM StopaPoreza s WHERE s.idStope = :idStope"),
    @NamedQuery(name = "StopaPoreza.findByStopa", query = "SELECT s FROM StopaPoreza s WHERE s.stopa = :stopa"),
    @NamedQuery(name = "StopaPoreza.findByDatumVazenja", query = "SELECT s FROM StopaPoreza s WHERE s.datumVazenja = :datumVazenja")})
public class StopaPoreza implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STOPE")
    private Integer idStope;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "STOPA")
    private float stopa;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_VAZENJA")
    @Temporal(TemporalType.DATE)
    private Date datumVazenja;
    
    @JoinColumn(name = "ID_POREZA", referencedColumnName = "ID_POREZA")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Porez idPoreza;

    public StopaPoreza() {
    }

 

    public StopaPoreza(float stopa, Date datumVazenja) {
        
        this.stopa = stopa;
        this.datumVazenja = datumVazenja;
    }

    public Integer getIdStope() {
    	
        return idStope;
    }

    public void setIdStope(Integer idStope) {
        this.idStope = idStope;
    }

    public float getStopa() {
        return stopa;
    }

    public void setStopa(float stopa) {
        this.stopa = stopa;
    }

    public Date getDatumVazenja() {
        return datumVazenja;
    }

    public void setDatumVazenja(Date datumVazenja) {
        this.datumVazenja = datumVazenja;
    }

    public Porez getIdPoreza() {
        return idPoreza;
    }

    public void setIdPoreza(Porez idPoreza) {
        this.idPoreza = idPoreza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStope != null ? idStope.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StopaPoreza)) {
            return false;
        }
        StopaPoreza other = (StopaPoreza) object;
        if ((this.idStope == null && other.idStope != null) || (this.idStope != null && !this.idStope.equals(other.idStope))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.StopaPoreza[ idStope=" + idStope + " ]";
    }
    
}
