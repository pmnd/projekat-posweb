/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Proizvod;

@Local
public interface ProizvodFacadeLocal {

    void create(Proizvod proizvod);

    void edit(Proizvod proizvod);

    void remove(Proizvod proizvod);

    Proizvod find(Object id);

    List<Proizvod> findAll();

    List<Proizvod> findRange(int[] range);

    int count();
    
}
