package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Porez;

@Local
public interface PorezFacadeLocal {

    void create(Porez porez);

    void edit(Porez porez);

    void remove(Porez porez);

    Porez find(Object id);

    List<Porez> findAll();

    List<Porez> findRange(int[] range);

    int count();
    
}
