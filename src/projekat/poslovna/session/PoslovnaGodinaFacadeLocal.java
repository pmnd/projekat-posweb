/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.PoslovnaGodina;

@Local
public interface PoslovnaGodinaFacadeLocal {

    void create(PoslovnaGodina poslovnaGodina);

    void edit(PoslovnaGodina poslovnaGodina);

    void remove(PoslovnaGodina poslovnaGodina);

    PoslovnaGodina find(Object id);

    List<PoslovnaGodina> findAll();

    List<PoslovnaGodina> findRange(int[] range);

    int count();
    
}
