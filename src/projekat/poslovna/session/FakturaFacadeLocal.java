/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Faktura;

@Local
public interface FakturaFacadeLocal {

    void create(Faktura faktura);

    void edit(Faktura faktura);

    void remove(Faktura faktura);

    Faktura find(Object id);

    List<Faktura> findAll();

    List<Faktura> findRange(int[] range);

    int count();
    
}
