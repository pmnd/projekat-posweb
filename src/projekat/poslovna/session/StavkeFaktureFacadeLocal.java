/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.StavkeFakture;

@Local
public interface StavkeFaktureFacadeLocal {

    void create(StavkeFakture stavkeFakture);

    void edit(StavkeFakture stavkeFakture);

    void remove(StavkeFakture stavkeFakture);

    StavkeFakture find(Object id);

    List<StavkeFakture> findAll();

    List<StavkeFakture> findRange(int[] range);

    int count();
    
}
