/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import projekat.poslovna.entity.Narudzbenica;

@Stateless
public class NarudzbenicaFacade extends AbstractFacade<Narudzbenica> implements NarudzbenicaFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NarudzbenicaFacade() {
        super(Narudzbenica.class);
    }

	@Override
	public int createFakturaSP(Integer id) {
		// TODO Auto-generated method stub
		
		//****************
		/*StoredProcedureQuery query = this.em.createNamedStoredProcedureQuery("calculate");
		query.setParameter("x", 1.23d);
		query.setParameter("y", 4.56d);
		query.execute();
		Double sum = (Double) query.getOutputParameterValue("sum");*/
		//****************
		
		Integer okParam = 1;
		
		Query query = this.em.createNativeQuery("DECLARE @return_value int, @OKParam int EXEC KreiranjeFaktureOdNarudzbine @IDNarudzbineParam = ?, @OK =  @OKParam OUTPUT");
		query.setParameter(1, id);
		

		query.executeUpdate();
		
		/*for (Object x : query.getResultList()) {
			System.out.println(x);
		}
		
		okParam = (Integer)query.getSingleResult();*/
		
		return okParam;
	}
    
}
