/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.StavkaNarudzbenice;


@Local
public interface StavkaNarudzbeniceFacadeLocal {

    void create(StavkaNarudzbenice stavkaNarudzbenice);

    void edit(StavkaNarudzbenice stavkaNarudzbenice);

    void remove(StavkaNarudzbenice stavkaNarudzbenice);

    StavkaNarudzbenice find(Object id);

    List<StavkaNarudzbenice> findAll();

    List<StavkaNarudzbenice> findRange(int[] range);

    int count();
    
}
