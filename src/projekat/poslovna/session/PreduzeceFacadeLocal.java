/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Preduzece;


@Local
public interface PreduzeceFacadeLocal {

    void create(Preduzece preduzece);

    void edit(Preduzece preduzece);

    void remove(Preduzece preduzece);

    Preduzece find(Object id);

    List<Preduzece> findAll();

    List<Preduzece> findRange(int[] range);

    int count();
    
}
