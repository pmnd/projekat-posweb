package projekat.poslovna.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import projekat.poslovna.entity.Korisnik;

@Stateless
public class KorisnikFacade extends AbstractFacade<Korisnik> implements KorisnikFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KorisnikFacade() {
        super(Korisnik.class);
    }

	@Override
	public Korisnik findByUsername(String username) throws Exception{

		Query query = em.createNamedQuery("Korisnik.findByUsername");
		query.setParameter("username", username);
		List k = new ArrayList<>(query.getResultList());
		if(k == null)
			return null;
		
		return (Korisnik) k.get(0);
	}

	@Override
	public Korisnik findByToken(String token)  throws Exception{
		Query query = em.createNamedQuery("Korisnik.findByToken");
		query.setParameter("token", token);
		Korisnik k = (Korisnik)query.getSingleResult();
		
		return k;
	}
}
