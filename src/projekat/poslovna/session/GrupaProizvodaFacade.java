/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projekat.poslovna.entity.GrupaProizvoda;


@Stateless
public class GrupaProizvodaFacade extends AbstractFacade<GrupaProizvoda> implements GrupaProizvodaFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GrupaProizvodaFacade() {
        super(GrupaProizvoda.class);
    }
    
}
