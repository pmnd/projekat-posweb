package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Korisnik;

@Local
public interface KorisnikFacadeLocal {

	 	void create(Korisnik korisnik);

	    void edit(Korisnik korisnik);

	    void remove(Korisnik korisnik);

	    Korisnik find(Object id);

	    List<Korisnik> findAll();

	    List<Korisnik> findRange(int[] range);
	    
	    Korisnik findByUsername(String username) throws Exception;


	    Korisnik findByToken(String token) throws Exception;
	    
	    int count();
}
