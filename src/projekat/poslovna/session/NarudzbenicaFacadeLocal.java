/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Narudzbenica;

@Local
public interface NarudzbenicaFacadeLocal {

    void create(Narudzbenica narudzbenica);

    void edit(Narudzbenica narudzbenica);

    void remove(Narudzbenica narudzbenica);

    Narudzbenica find(Object id);

    List<Narudzbenica> findAll();

    List<Narudzbenica> findRange(int[] range);

    int count();
    
    int createFakturaSP(Integer id);
    
}
