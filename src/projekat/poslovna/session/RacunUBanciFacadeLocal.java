/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.RacunUBanci;


@Local
public interface RacunUBanciFacadeLocal {

    void create(RacunUBanci racunUBanci);

    void edit(RacunUBanci racunUBanci);

    void remove(RacunUBanci racunUBanci);

    RacunUBanci find(Object id);

    List<RacunUBanci> findAll();

    List<RacunUBanci> findRange(int[] range);

    int count();
    
}
