/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.GrupaProizvoda;

@Local
public interface GrupaProizvodaFacadeLocal {

    void create(GrupaProizvoda grupaProizvoda);

    void edit(GrupaProizvoda grupaProizvoda);

    void remove(GrupaProizvoda grupaProizvoda);

    GrupaProizvoda find(Object id);

    List<GrupaProizvoda> findAll();

    List<GrupaProizvoda> findRange(int[] range);

    int count();
    
}
