/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.ObracunatiPorezi;

@Local
public interface ObracunatiPoreziFacadeLocal {

    void create(ObracunatiPorezi obracunatiPorezi);

    void edit(ObracunatiPorezi obracunatiPorezi);

    void remove(ObracunatiPorezi obracunatiPorezi);

    ObracunatiPorezi find(Object id);

    List<ObracunatiPorezi> findAll();

    List<ObracunatiPorezi> findRange(int[] range);

    int count();
    
}
