/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.PoslovniPartner;

@Local
public interface PoslovniPartnerFacadeLocal {

    void create(PoslovniPartner poslovniPartner);

    void edit(PoslovniPartner poslovniPartner);

    void remove(PoslovniPartner poslovniPartner);

    PoslovniPartner find(Object id);

    List<PoslovniPartner> findAll();

    List<PoslovniPartner> findRange(int[] range);

    int count();
    
}
