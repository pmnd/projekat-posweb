package projekat.poslovna.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projekat.poslovna.entity.Porez;

@Stateless
public class PorezFacade extends AbstractFacade<Porez> implements PorezFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PorezFacade() {
        super(Porez.class);
    }
    
}