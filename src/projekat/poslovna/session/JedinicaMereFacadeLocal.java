/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.JedinicaMere;


@Local
public interface JedinicaMereFacadeLocal {

    void create(JedinicaMere jedinicaMere);

    void edit(JedinicaMere jedinicaMere);

    void remove(JedinicaMere jedinicaMere);

    JedinicaMere find(Object id);

    List<JedinicaMere> findAll();

    List<JedinicaMere> findRange(int[] range);

    int count();
    
}
