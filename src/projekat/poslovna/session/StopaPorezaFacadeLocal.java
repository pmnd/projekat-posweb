/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.StopaPoreza;

@Local
public interface StopaPorezaFacadeLocal {

    void create(StopaPoreza stopaPoreza);

    void edit(StopaPoreza stopaPoreza);

    void remove(StopaPoreza stopaPoreza);

    StopaPoreza find(Object id);

    List<StopaPoreza> findAll();

    List<StopaPoreza> findRange(int[] range);

    int count();
    
}
