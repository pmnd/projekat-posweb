/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import projekat.poslovna.entity.Cenovnik;

@Stateless
public class CenovnikFacade extends AbstractFacade<Cenovnik> implements CenovnikFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CenovnikFacade() {
        super(Cenovnik.class);
    }

	@Override
	public int kopiranjeCenovnikaSP(int idStarogCenovnika, String nazivCenovnika, Date vaziOd, float procenat) {
		Integer okParam = 1;
		
		Query query = this.em.createNativeQuery("DECLARE @return_value int, @OKParam int EXEC @return_value = [dbo].[KopiranjeCenovnika] @IDCenovnikaParam = ?, @nazivParam = ?, @vaziOdParam = ?, @procenatParam = ?, @OK = @OKParam OUTPUT");
		query.setParameter(1, idStarogCenovnika);
		query.setParameter(2, nazivCenovnika);
		query.setParameter(3, vaziOd);
		query.setParameter(4, procenat);
		

		query.executeUpdate();
		
		/*for (Object x : query.getResultList()) {
			System.out.println(x);
		}
		
		okParam = (Integer)query.getSingleResult();*/
		
		return okParam;
	}
    
}
