/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.StavkeCenovnika;


@Local
public interface StavkeCenovnikaFacadeLocal {

    void create(StavkeCenovnika stavkeCenovnika);

    void edit(StavkeCenovnika stavkeCenovnika);

    void remove(StavkeCenovnika stavkeCenovnika);

    StavkeCenovnika find(Object id);

    List<StavkeCenovnika> findAll();

    List<StavkeCenovnika> findRange(int[] range);

    int count();
    
}
