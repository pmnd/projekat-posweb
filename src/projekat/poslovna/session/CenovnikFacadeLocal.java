/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import projekat.poslovna.entity.Cenovnik;


@Local
public interface CenovnikFacadeLocal {

    void create(Cenovnik cenovnik);

    void edit(Cenovnik cenovnik);

    void remove(Cenovnik cenovnik);

    Cenovnik find(Object id);

    List<Cenovnik> findAll();

    List<Cenovnik> findRange(int[] range);

    int count();
    
    int kopiranjeCenovnikaSP(int idStarogCenovnika, String nazivCenovnika, Date vaziOd, float procenat);
    
}
