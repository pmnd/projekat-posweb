/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekat.poslovna.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projekat.poslovna.entity.RacunUBanci;

@Stateless
public class RacunUBanciFacade extends AbstractFacade<RacunUBanci> implements RacunUBanciFacadeLocal {

    @PersistenceContext(unitName = "POSLOVNA")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RacunUBanciFacade() {
        super(RacunUBanci.class);
    }
    
}
