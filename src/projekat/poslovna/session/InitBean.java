package projekat.poslovna.session;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projekat.poslovna.entity.JedinicaMere;

@Stateless
@Remote(Init.class)
public class InitBean implements Init {

	@PersistenceContext(unitName = "POSLOVNA")
	EntityManager em;
	@EJB
	JedinicaMereFacadeLocal jm;
	
	public void init() {
		JedinicaMere p = new JedinicaMere("Jedinica", "init");
		jm.create(p);
	}
}
