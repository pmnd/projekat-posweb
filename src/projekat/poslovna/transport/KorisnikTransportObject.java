package projekat.poslovna.transport;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Korisnik;

@XmlRootElement(name="korisnik")
public class KorisnikTransportObject {
	
    public String username;
    public String password;
    
    
    public KorisnikTransportObject() {
		super();
	
	}
    public KorisnikTransportObject(Korisnik korisnik){
    	 username = korisnik.getUsername();
    	 password = korisnik.getPassword();
	}
}
