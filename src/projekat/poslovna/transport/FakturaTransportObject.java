package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Faktura;

@XmlRootElement(name="faktura")
public class FakturaTransportObject {
	
	    public Integer idFakture;
	    public int brojFakture;
	    public Date datumFakture;
	    public Date datumPlacanjaFakture;
	    public int status;
	    public float ukupanIznosBezPdvA;
	    public float ukupanPdv;
	    public float ukupanRabat;
	    public float ukupnoZaPlacanje;
	    public Integer idPoslovnogPartnera;
	    public Integer idGodine;
	    public Integer idNarudzbenice;
	    public Integer idRacuna;
	    
	    public String naziv; //poslovnog partnera
	    public Integer godina;
	    public Integer brojNarudzbenice;
	    public String brojRacuna;
	    
		public FakturaTransportObject(Integer idFakture, int brojFakture, Date datumFakture, Date datumPlacanjaFakture,
				float ukupanRabat, float ukupanIznosBezPdvA, float ukupanPdv, float ukupnoZaPlacanje, int status,
				Integer idPoslovnogPartnera,Integer godina,Integer brojNarudzbenice, Integer idNarudzbenice,String naziv, Integer idGodine, Integer idRacuna,String brojRacuna) {
			super();
			this.idFakture = idFakture;
			this.brojFakture = brojFakture;
			this.datumFakture = datumFakture;
			this.datumPlacanjaFakture = datumPlacanjaFakture;
			this.ukupanRabat = ukupanRabat;
			this.ukupanIznosBezPdvA = ukupanIznosBezPdvA;
			this.ukupanPdv = ukupanPdv;
			this.ukupnoZaPlacanje = ukupnoZaPlacanje;
			this.status = status;
			this.idPoslovnogPartnera = idPoslovnogPartnera;
			this.idNarudzbenice = idNarudzbenice;
			this.brojNarudzbenice = brojNarudzbenice;
			this.idGodine = idGodine;
			this.godina = godina;
			this.idRacuna = idRacuna;
			this.brojRacuna = brojRacuna;
			this.naziv = naziv;
		}
		public FakturaTransportObject() {
			super();
		}    
	    
		public static List<FakturaTransportObject> getListOfDtos(Collection<Faktura> param){
			List<FakturaTransportObject> temp = new ArrayList<FakturaTransportObject>();
			
			for(Faktura n : param){
				temp.add(new FakturaTransportObject(n));
			}
			
			return temp;
		}
		
		public FakturaTransportObject(Faktura param){
			this.idFakture = param.getIdFakture();
			this.brojFakture = param.getBrojFakture();
			this.datumFakture = param.getDatumFakture();
			this.datumPlacanjaFakture = param.getDatumPlacanjaFakture();
			this.ukupanRabat = param.getUkupanRabat();
			this.ukupanIznosBezPdvA = param.getUkupanIznosBezPdvA();
			this.ukupanPdv = param.getUkupanPdv();
			this.ukupnoZaPlacanje = param.getUkupnoZaPlacanje();
			this.status = param.getStatus();
			this.idPoslovnogPartnera = param.getIdPoslovnogPartnera().getIdPoslovnogPartnera();
			this.idNarudzbenice = param.getIdNarudzbenice().getIdNarudzbenice();
			this.idGodine = param.getIdGodine().getIdGodine();
			this.naziv = param.getIdPoslovnogPartnera().getNaziv();
			this.brojNarudzbenice = param.getIdNarudzbenice().getBrojNarudzbenice();
			this.godina = param.getIdGodine().getGodina();
			if(param.getIdRacunaUBanci() != null){
				this.idRacuna = param.getIdRacunaUBanci().getIdRacuna();
				this.brojRacuna = param.getIdRacunaUBanci().getBrojRacuna();
			}else{
				this.idRacuna = null;
				this.brojRacuna = null;
			}
		}

}
