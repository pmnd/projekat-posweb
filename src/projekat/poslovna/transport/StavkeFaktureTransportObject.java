package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.StavkeFakture;

@XmlRootElement(name="stavke_fakture")
public class StavkeFaktureTransportObject {

    public Integer idStavke;
    public float iznosPdvA;
    public float jedinicnaCena;
    public float kolicina;
    public float osnovica;
    public float rabat;
    public float stopaPdvA;
    public float ukupanIznos;
    public Integer idFakture;
    public Integer idProizvoda;
    
    public Integer brojFakture;
    public String nazivProizvoda;

	public StavkeFaktureTransportObject(Integer idStavke, float kolicina, float rabat, float jedinicnaCena,
			float stopaPdvA, float osnovica, float iznosPdvA, float ukupanIznos, Integer idFakture,
			Integer idProizvoda) {
		super();
		this.idStavke = idStavke;
		this.kolicina = kolicina;
		this.rabat = rabat;
		this.jedinicnaCena = jedinicnaCena;
		this.stopaPdvA = stopaPdvA;
		this.osnovica = osnovica;
		this.iznosPdvA = iznosPdvA;
		this.ukupanIznos = ukupanIznos;
		this.idFakture = idFakture;
		this.idProizvoda = idProizvoda;
	}

	public StavkeFaktureTransportObject() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
	public static List<StavkeFaktureTransportObject> getListOfDtos(Collection<StavkeFakture> param){
		List<StavkeFaktureTransportObject> temp = new ArrayList<StavkeFaktureTransportObject>();
		
		for(StavkeFakture n : param){
			temp.add(new StavkeFaktureTransportObject(n));
		}
		
		return temp;
	}
	
	public StavkeFaktureTransportObject(StavkeFakture param){
		this.idStavke = param.getIdStavke();
		this.kolicina = param.getKolicina();
		this.rabat = param.getRabat();
		this.jedinicnaCena = param.getJedinicnaCena();
		this.stopaPdvA = param.getStopaPdvA();
		this.osnovica = param.getOsnovica();
		this.iznosPdvA = param.getIznosPdvA();
		this.ukupanIznos = param.getUkupanIznos();
		this.idFakture = param.getIdFakture().getIdFakture();
		this.idProizvoda = param.getIdProizvoda().getIdProizvoda();
		this.brojFakture = param.getIdFakture().getBrojFakture();
		this.nazivProizvoda = param.getIdProizvoda().getNazivProizvoda();
	}
}
