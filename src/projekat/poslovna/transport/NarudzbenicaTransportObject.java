package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Narudzbenica;

@XmlRootElement(name="narudzbenica")
public class NarudzbenicaTransportObject {
	public Integer idNarudzbenice;
	public int brojNarudzbenice;
    public Date datum;
    public int prihvacena;
    public Integer idGodine;
    public Integer idPoslovnogPartnera;
    
    public Integer godina;
    public String naziv;
    
	public NarudzbenicaTransportObject(Integer idNarudzbenice, int brojNarudzbenice, Date datum, int prihvacena,
			Integer idGodine, Integer idPoslovnogPartnera,String naziv) {
		super();
		this.idNarudzbenice = idNarudzbenice;
		this.brojNarudzbenice = brojNarudzbenice;
		this.datum = datum;
		this.prihvacena = prihvacena;
		this.idGodine = idGodine;
		this.idPoslovnogPartnera = idPoslovnogPartnera;
		this.naziv = naziv;
	}

	public NarudzbenicaTransportObject() {
		super();
	}
	
	public static List<NarudzbenicaTransportObject> getListOfDtos(Collection<Narudzbenica> param){
		List<NarudzbenicaTransportObject> temp = new ArrayList<NarudzbenicaTransportObject>();
		
		for(Narudzbenica n : param){
			temp.add(new NarudzbenicaTransportObject(n));
		}
		
		return temp;
	}
	
	public NarudzbenicaTransportObject(Narudzbenica narudzbenica){
		idNarudzbenice = narudzbenica.getIdNarudzbenice();
		brojNarudzbenice = narudzbenica.getBrojNarudzbenice();
		datum = narudzbenica.getDatum();
		prihvacena = narudzbenica.getPrihvacena();
		idGodine = narudzbenica.getIdGodine().getIdGodine();
		godina = narudzbenica.getIdGodine().getGodina();
		idPoslovnogPartnera = narudzbenica.getIdPoslovnogPartnera().getIdPoslovnogPartnera();
		naziv = narudzbenica.getIdPoslovnogPartnera().getNaziv();
	}
}
