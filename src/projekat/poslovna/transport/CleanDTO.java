package projekat.poslovna.transport;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "cleanDTO")
public class CleanDTO {
	public String text;
	
	public CleanDTO() {}
	
	public CleanDTO(String text) {
		this.text = text;
	}
	
}
