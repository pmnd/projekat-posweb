package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.GrupaProizvoda;

@XmlRootElement(name="grupa_proizvoda")
public class GrupaProizvodaTransportObject {
    public Integer idGrupe;
    public String nazivGrupe;
    public Integer idPreduzeca;
    public Integer idPoreza;
    

    public String nazivPreduzeca;
    public String nazivPoreza;
    
	public GrupaProizvodaTransportObject(Integer idGrupe, String nazivGrupe, Integer idPreduzeca, Integer idPoreza) {
		super();
		this.idGrupe = idGrupe;
		this.nazivGrupe = nazivGrupe;
		this.idPreduzeca = idPreduzeca;
		this.idPoreza = idPoreza;
	}
	public GrupaProizvodaTransportObject() {
		super();
	
	}
	
	public static List<GrupaProizvodaTransportObject> getListOfDtos(Collection<GrupaProizvoda> param){
		List<GrupaProizvodaTransportObject> temp = new ArrayList<GrupaProizvodaTransportObject>();
		
		for(GrupaProizvoda n : param){
			temp.add(new GrupaProizvodaTransportObject(n));
		}
		
		return temp;
	}
	
	public GrupaProizvodaTransportObject(GrupaProizvoda param){
		this.idGrupe = param.getIdGrupe();
		this.nazivGrupe = param.getNazivGrupe();
		this.idPreduzeca = param.getIdPreduzeca().getIdPreduzeca();
		this.idPoreza = param.getIdPoreza().getIdPoreza();
		this.nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
		this.nazivPoreza = param.getIdPoreza().getNazivPoreza();
	}
    
    
}
