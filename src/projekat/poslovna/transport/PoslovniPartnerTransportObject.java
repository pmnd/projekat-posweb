package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.PoslovniPartner;

@XmlRootElement(name="poslovni_partner")
public class PoslovniPartnerTransportObject {
	public Integer idPoslovnogPartnera;
	public String adresa;
	public String naziv;
	public String pib;
	public String vrsta;
	public Integer idPreduzeca;
	
	public String nazivPreduzeca;
	
	public PoslovniPartnerTransportObject(Integer idPoslovnogPartnera, String naziv, String adresa, String pib, String vrsta, Integer idPreduzeca) {
		super();
		this.idPoslovnogPartnera = idPoslovnogPartnera;
		this.naziv = naziv;
		this.adresa = adresa;
		this.pib = pib;
		this.vrsta = vrsta;
		this.idPreduzeca = idPreduzeca;
	}
	public PoslovniPartnerTransportObject() {
		super();
	}
	
	public static List<PoslovniPartnerTransportObject> getListOfDtos(Collection<PoslovniPartner> param){
		List<PoslovniPartnerTransportObject> temp = new ArrayList<PoslovniPartnerTransportObject>();
		
		for(PoslovniPartner n : param){
			temp.add(new PoslovniPartnerTransportObject(n));
		}
		
		return temp;
	}
		
	public PoslovniPartnerTransportObject(PoslovniPartner param){
		this.idPoslovnogPartnera = param.getIdPoslovnogPartnera();
		this.naziv = param.getNaziv();
		this.adresa = param.getAdresa();
		this.pib = param.getPib();
		this.idPoslovnogPartnera = param.getIdPoslovnogPartnera();
		this.vrsta = param.getVrsta();
		this.idPreduzeca = param.getIdPreduzeca().getIdPreduzeca();
		this.nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
    
}
