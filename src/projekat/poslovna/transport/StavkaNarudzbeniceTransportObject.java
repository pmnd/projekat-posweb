package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.StavkaNarudzbenice;

@XmlRootElement(name="stavka_narudzbenice")
public class StavkaNarudzbeniceTransportObject {
	
    public Integer idStavkeNarudzbenice;
    public float jedinicnaCenaStavkeNar; 
    public float kolicinaStavkeNar;
    public Integer idNarudzbenice;
    public Integer idProizvoda;
    

    public Integer brojNarudzbenice;
    public String nazivProizvoda;
    
	public StavkaNarudzbeniceTransportObject(Integer idStavkeNarudzbenice, float kolicinaStavkeNar,
			float jedinicnaCenaStavkeNar, Integer idNarudzbenice, Integer idProizvoda) {
		super();
		this.idStavkeNarudzbenice = idStavkeNarudzbenice;
		this.kolicinaStavkeNar = kolicinaStavkeNar;
		this.jedinicnaCenaStavkeNar = jedinicnaCenaStavkeNar;
		this.idNarudzbenice = idNarudzbenice;
		this.idProizvoda = idProizvoda;
	}
	public StavkaNarudzbeniceTransportObject() {
		super();
	}
    
	public static List<StavkaNarudzbeniceTransportObject> getListOfDtos(Collection<StavkaNarudzbenice> param){
		List<StavkaNarudzbeniceTransportObject> temp = new ArrayList<StavkaNarudzbeniceTransportObject>();
		
		for(StavkaNarudzbenice n : param){
			temp.add(new StavkaNarudzbeniceTransportObject(n));
		}
		
		return temp;
	}
	
	public StavkaNarudzbeniceTransportObject(StavkaNarudzbenice param){
		idNarudzbenice = param.getIdNarudzbenice().getIdNarudzbenice();
		kolicinaStavkeNar = param.getKolicinaStavkeNar();
		jedinicnaCenaStavkeNar = param.getJedinicnaCenaStavkeNar();
		idStavkeNarudzbenice = param.getIdStavkeNarudzbenice();
		idProizvoda = param.getIdProizvoda().getIdProizvoda();	

		brojNarudzbenice = param.getIdNarudzbenice().getBrojNarudzbenice();
		nazivProizvoda = param.getIdProizvoda().getNazivProizvoda();	
	}
	
    
	
	
}
