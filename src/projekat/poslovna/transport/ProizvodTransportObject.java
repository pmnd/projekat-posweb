package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Proizvod;

@XmlRootElement(name="proizvod")
public class ProizvodTransportObject {
	public Integer idProizvoda;
	public float jedinicnaKolicina;
	public String nazivProizvoda;
	public Integer idGrupe;
	public Integer idJedinice;

	public String nazivGrupe;
	public String nazivJediniceMere;
	
	public ProizvodTransportObject(Integer idProizvoda, String nazivProizvoda, float jedinicnaKolicina, Integer idGrupe,
			Integer idJedinice, String nazivJediniceMere) {
		super();
		this.idProizvoda = idProizvoda;
		this.nazivProizvoda = nazivProizvoda;
		this.jedinicnaKolicina = jedinicnaKolicina;
		this.idGrupe = idGrupe;
		this.idJedinice = idJedinice;
		this.nazivJediniceMere = nazivJediniceMere;
	}

	public ProizvodTransportObject() {
		super();
	
	}
	
	public static List<ProizvodTransportObject> getListOfDtos(Collection<Proizvod> param){
		List<ProizvodTransportObject> temp = new ArrayList<ProizvodTransportObject>();
		
		for(Proizvod n : param){
			temp.add(new ProizvodTransportObject(n));
		}
		
		return temp;
	}
	
	public ProizvodTransportObject(Proizvod param){
		this.idProizvoda = param.getIdProizvoda();
		this.nazivProizvoda = param.getNazivProizvoda();
		this.jedinicnaKolicina = param.getJedinicnaKolicina();
		this.idGrupe = param.getIdGrupe().getIdGrupe();
		this.idJedinice = param.getIdJedinice().getIdJedinice();
		this.nazivGrupe = param.getIdGrupe().getNazivGrupe();
		this.nazivJediniceMere = param.getIdJedinice().getNazivJediniceMere();
	}
	
   
}
