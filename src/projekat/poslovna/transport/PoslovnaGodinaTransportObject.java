package projekat.poslovna.transport;

import java.util.ArrayList;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.PoslovnaGodina;

@XmlRootElement(name="poslovna_godina")
public class PoslovnaGodinaTransportObject {

	public Integer idGodine;
	public Short daLiJeZakljucena;
	public int godina;
	public Integer idPreduzeca;

	public String nazivPreduzeca;
	
	public PoslovnaGodinaTransportObject(Integer idGodine, int godina, Short daLiJeZakljucena, Integer idPreduzeca) {
		super();
		this.idGodine = idGodine;
		this.godina = godina;
		this.daLiJeZakljucena = daLiJeZakljucena;
		this.idPreduzeca = idPreduzeca;
	}

	public PoslovnaGodinaTransportObject() {
		super();
	}
	
	public static List<PoslovnaGodinaTransportObject> getListOfDtos(Collection<PoslovnaGodina> param){
		List<PoslovnaGodinaTransportObject> temp = new ArrayList<PoslovnaGodinaTransportObject>();
		
		for(PoslovnaGodina n : param){
			temp.add(new PoslovnaGodinaTransportObject(n));
		}
		
		return temp;
	}
	
	public PoslovnaGodinaTransportObject(PoslovnaGodina param){
		idGodine = param.getIdGodine();
		godina = param.getGodina();
		daLiJeZakljucena = param.getDaLiJeZakljucena();
		idPreduzeca = param.getIdPreduzeca().getIdPreduzeca();
		this.nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
	
	
}
