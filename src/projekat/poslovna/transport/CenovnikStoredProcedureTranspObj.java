package projekat.poslovna.transport;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name ="cenovnik")
public class CenovnikStoredProcedureTranspObj {
	//int idStarogCenovnika, String nazivCenovnika, Date vaziOd, float procenat
	public Integer idStarogCenovnika;
	public String nazivCenovnika;
	public Date vaziOd;
	public float procenat;
	
	public CenovnikStoredProcedureTranspObj() {}
	
	public CenovnikStoredProcedureTranspObj(Integer idStarogCenovnika, String nazivCenovnika, Date vaziOd,
			float procenat) {
		super();
		this.idStarogCenovnika = idStarogCenovnika;
		this.nazivCenovnika = nazivCenovnika;
		this.vaziOd = vaziOd;
		this.procenat = procenat;
	}
	
}
