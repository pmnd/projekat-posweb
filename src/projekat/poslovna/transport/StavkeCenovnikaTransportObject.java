package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.StavkeCenovnika;

@XmlRootElement(name="stavke_cenovnika")
public class StavkeCenovnikaTransportObject {

    public Integer idStavkeCenovnika;
    public float cena;
    public Integer idCenovnika;
    public Integer idProizvoda;
    
    public String nazivCenovnika;
    public String nazivProizvoda;
    
	public StavkeCenovnikaTransportObject(Integer idStavkeCenovnika, float cena, Integer idProizvoda,
			Integer idCenovnika,String nazivCenovnika) {
		super();
		this.idStavkeCenovnika = idStavkeCenovnika;
		this.cena = cena;
		this.idProizvoda = idProizvoda;
		this.idCenovnika = idCenovnika;
		this.nazivCenovnika = nazivCenovnika;
	}
	public StavkeCenovnikaTransportObject() {
		super();
	
	}
    
	
	public static List<StavkeCenovnikaTransportObject> getListOfDtos(Collection<StavkeCenovnika> param){
		List<StavkeCenovnikaTransportObject> temp = new ArrayList<StavkeCenovnikaTransportObject>();
		
		for(StavkeCenovnika n : param){
			temp.add(new StavkeCenovnikaTransportObject(n));
		}
		
		return temp;
	}
	
	public StavkeCenovnikaTransportObject(StavkeCenovnika param){
		this.idStavkeCenovnika = param.getIdStavkeCenovnika();
		this.cena = param.getCena();
		this.idProizvoda = param.getIdProizvoda().getIdProizvoda();
		this.idCenovnika = param.getIdCenovnika().getIdCenovnika();

		this.nazivProizvoda = param.getIdProizvoda().getNazivProizvoda();
		this.nazivCenovnika = param.getIdCenovnika().getNazivCenovnika();
	}
}
