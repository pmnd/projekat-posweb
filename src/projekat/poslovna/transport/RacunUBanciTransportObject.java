package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.RacunUBanci;

@XmlRootElement(name="racun_u_banci")
public class RacunUBanciTransportObject {
    public Integer idRacuna;
    public String brojRacuna;
    public String nazivBanke;
    public Integer idPreduzeca;

    public String nazivPreduzeca;
    
	public RacunUBanciTransportObject(Integer idRacuna, String brojRacuna, String nazivBanke, Integer idPreduzeca) {
		super();
		this.idRacuna = idRacuna;
		this.brojRacuna = brojRacuna;
		this.nazivBanke = nazivBanke;
		this.idPreduzeca = idPreduzeca;
	}
	public RacunUBanciTransportObject() {
		super();
	}
	
	public static List<RacunUBanciTransportObject> getListOfDtos(Collection<RacunUBanci> param){
		List<RacunUBanciTransportObject> temp = new ArrayList<RacunUBanciTransportObject>();
		
		for(RacunUBanci n : param){
			temp.add(new RacunUBanciTransportObject(n));
		}
		
		return temp;
	}
	
	public RacunUBanciTransportObject(RacunUBanci param){
		idRacuna = param.getIdRacuna();
		brojRacuna = param.getBrojRacuna();
		nazivBanke = param.getNazivBanke();
		idPreduzeca = param.getIdPreduzeca().getIdPreduzeca();
		nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
    
    

}
