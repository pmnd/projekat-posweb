package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Preduzece;

@XmlRootElement(name="preduzece")
public class PreduzeceTransportObject {
    public Integer idPreduzeca;
    public String adresa;
    public String nazivPreduzeca;
    public String pib;
    
	public PreduzeceTransportObject(Integer idPreduzeca, String nazivPreduzeca, String adresa, String pib) {
		super();
		this.idPreduzeca = idPreduzeca;
		this.nazivPreduzeca = nazivPreduzeca;
		this.adresa = adresa;
		this.pib = pib;
	}
	
	public PreduzeceTransportObject() {
		super();
	}
	
	public static List<PreduzeceTransportObject> getListOfDtos(Collection<Preduzece> param){
		List<PreduzeceTransportObject> temp = new ArrayList<PreduzeceTransportObject>();
		
		for(Preduzece n : param){
			temp.add(new PreduzeceTransportObject(n));
		}
		
		return temp;
	}
	
	public PreduzeceTransportObject(Preduzece param){
		idPreduzeca = param.getIdPreduzeca();
		nazivPreduzeca = param.getNazivPreduzeca();
		adresa = param.getAdresa();
		pib = param.getPib();
	}
}
