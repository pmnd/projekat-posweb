package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.ObracunatiPorezi;

@XmlRootElement(name="obracunati_porezi")
public class ObracunatiPoreziTransportObject {
    public Integer idObracunatogPoreza;
    public float iznos;
    public Integer idFakture;
    public Integer idPoreza;
    
    public Integer brojFakture;
    public String nazivPoreza;
    
	public ObracunatiPoreziTransportObject(Integer idObracunatogPoreza, float iznos, Integer idFakture,
			Integer idPoreza) {
		super();
		this.idObracunatogPoreza = idObracunatogPoreza;
		this.iznos = iznos;
		this.idFakture = idFakture;
		this.idPoreza = idPoreza;
	}
	public ObracunatiPoreziTransportObject() {
		super();
	}
	
	public static List<ObracunatiPoreziTransportObject> getListOfDtos(Collection<ObracunatiPorezi> param){
		List<ObracunatiPoreziTransportObject> temp = new ArrayList<ObracunatiPoreziTransportObject>();
		
		for(ObracunatiPorezi n : param){
			temp.add(new ObracunatiPoreziTransportObject(n));
		}
		
		return temp;
	}
	
	public ObracunatiPoreziTransportObject(ObracunatiPorezi param){
		this.idObracunatogPoreza = param.getIdObracunatogPoreza();
		this.iznos = param.getIznos();
		this.idFakture = param.getIdFakture().getIdFakture();
		this.idPoreza = param.getIdPoreza().getIdPoreza();
		this.brojFakture = param.getIdFakture().getBrojFakture();
		this.nazivPoreza = param.getIdPoreza().getNazivPoreza();
	}
    
    
    

}
