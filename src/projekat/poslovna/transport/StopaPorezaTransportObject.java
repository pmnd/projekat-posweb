package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.StopaPoreza;

@XmlRootElement(name="stopa_poreza")
public class StopaPorezaTransportObject {
    public Integer idStope;
    public Date datumVazenja;
    public float stopa;
    public Integer idPoreza;
    
    public String nazivPoreza;
    
	public StopaPorezaTransportObject(Integer idStope, float stopa, Date datumVazenja, Integer idPoreza) {
		super();
		this.idStope = idStope;
		this.stopa = stopa;
		this.datumVazenja = datumVazenja;
		this.idPoreza = idPoreza;
	}
	public StopaPorezaTransportObject() {
		super();
	
	}
    
	public static List<StopaPorezaTransportObject> getListOfDtos(Collection<StopaPoreza> param){
		List<StopaPorezaTransportObject> temp = new ArrayList<StopaPorezaTransportObject>();
		
		for(StopaPoreza n : param){
			temp.add(new StopaPorezaTransportObject(n));
		}
		
		return temp;
	}
	
	public StopaPorezaTransportObject(StopaPoreza param){
		this.idStope = param.getIdStope();
		this.stopa = param.getStopa();
		this.datumVazenja = param.getDatumVazenja();
		this.idPoreza = param.getIdPoreza().getIdPoreza();
		this.nazivPoreza = param.getIdPoreza().getNazivPoreza();
	}
    
}
