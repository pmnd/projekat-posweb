package projekat.poslovna.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Cenovnik;

@XmlRootElement(name="cenovnik")
public class CenovnikTransportObject {
	
    public Integer idCenovnika;
    public Date vaziOd;
    public String nazivCenovnika;
    public Integer idPreduzeca;
    
    public String nazivPreduzeca;
    
	public CenovnikTransportObject(Integer idCenovnika, Date vaziOd, String nazivCenovnika, Integer idPreduzeca, String nazivPreduzeca) {
		super();
		this.idCenovnika = idCenovnika;
		this.vaziOd = vaziOd;
		this.idPreduzeca = idPreduzeca;
		this.nazivCenovnika = nazivCenovnika;
		this.nazivPreduzeca = nazivPreduzeca;
	}
	public CenovnikTransportObject() {
		super();
	
	}
	public static List<CenovnikTransportObject> getListOfDtos(Collection<Cenovnik> param){
		List<CenovnikTransportObject> temp = new ArrayList<CenovnikTransportObject>();
		
		for(Cenovnik n : param){
			temp.add(new CenovnikTransportObject(n));
		}
		
		return temp;
	}
	
	public CenovnikTransportObject(Cenovnik param){
		this.idCenovnika = param.getIdCenovnika();
		this.vaziOd = param.getVaziOd();
		this.nazivCenovnika = param.getNazivCenovnika();
		this.idPreduzeca = param.getIdPreduzeca().getIdPreduzeca();
		this.nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
    
    
    
   
}
