package projekat.poslovna.xml;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.ObracunatiPorezi;

@XmlRootElement(name = "obracunatPorez")
public class ObracunatiPoreziXML {
	public Integer idObracunatogPoreza;
	public float iznos;
	public int brojFakture;
	public String nazivPoreza;
	
	public ObracunatiPoreziXML() {}
	
	public ObracunatiPoreziXML(ObracunatiPorezi param) {
		idObracunatogPoreza = param.getIdObracunatogPoreza();
		iznos = param.getIznos();
		brojFakture = param.getIdFakture().getBrojFakture();
		nazivPoreza = param.getIdPoreza().getNazivPoreza();
	}
}
