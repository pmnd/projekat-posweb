package projekat.poslovna.xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Faktura;
import projekat.poslovna.entity.ObracunatiPorezi;
import projekat.poslovna.entity.StavkeFakture;

@XmlRootElement(name = "faktura")
public class FakturaXML {
	public Integer id;
	public int brojFakture;
	public Date datumFakture;
	public Date datumPlacanjaFakture;
	public float ukupanRabat;
	public float ukupanIznosBezPdvA;
	public float ukupanPdv;
	public float ukupnoZaPlacanje;
	public int status;
	public List<StavkeFaktureXML> stavkeFakture;
	public PoslovniPartnerXML poslovniPartner;
	public RacunUBanciXML racunUBanci;
	public NarudzbenicaXML narudzbenica;
	public PoslovnaGodinaXML poslovnaGodina;
	public List<ObracunatiPoreziXML> obracunatiPorezi;

	public FakturaXML() {
		
	}
	
	public FakturaXML(Faktura param) {
		id = param.getIdFakture();
		brojFakture = param.getBrojFakture();
		datumFakture = param.getDatumFakture();
		datumPlacanjaFakture = param.getDatumPlacanjaFakture();
		ukupanRabat = param.getUkupanRabat();
		ukupanIznosBezPdvA = param.getUkupanIznosBezPdvA();
		ukupanPdv = param.getUkupanPdv();
		ukupnoZaPlacanje = param.getUkupnoZaPlacanje();
		status = param.getStatus();
		stavkeFakture = new ArrayList<StavkeFaktureXML>();
		for (StavkeFakture tempStavkaFaktura : param.getStavkeFaktureSet()) {
			stavkeFakture.add(new StavkeFaktureXML(tempStavkaFaktura));
		}
		poslovniPartner = new PoslovniPartnerXML(param.getIdPoslovnogPartnera());
		racunUBanci = new RacunUBanciXML(param.getIdRacunaUBanci());
		narudzbenica = new NarudzbenicaXML(param.getIdNarudzbenice());
		poslovnaGodina = new PoslovnaGodinaXML(param.getIdGodine());
		obracunatiPorezi = new ArrayList<ObracunatiPoreziXML>();
		for (ObracunatiPorezi tempObracunatiPorez : param.getObracunatiPoreziSet()) {
			obracunatiPorezi.add(new ObracunatiPoreziXML(tempObracunatiPorez));
		}
	}

}
