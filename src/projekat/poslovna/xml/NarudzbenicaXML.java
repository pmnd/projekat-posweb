package projekat.poslovna.xml;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.Narudzbenica;

@XmlRootElement(name = "narudzbenica")
public class NarudzbenicaXML {
	public Integer idNarudzbenice;
	public int brojNarudzbenice;
	public Date datum;
	public int prihvacena;
	public int brojPoslovneGodine;
	public String nazivPoslovnogPartnera;

	public NarudzbenicaXML() {}
	
	public NarudzbenicaXML(Narudzbenica param) {
		idNarudzbenice = param.getIdNarudzbenice();
		brojNarudzbenice = param.getBrojNarudzbenice();
		datum = param.getDatum();
		prihvacena = param.getPrihvacena();
		brojPoslovneGodine = param.getIdGodine().getGodina();
		nazivPoslovnogPartnera = param.getIdPoslovnogPartnera().getNaziv();
	}

}
