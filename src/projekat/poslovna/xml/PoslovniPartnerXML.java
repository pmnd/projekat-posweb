package projekat.poslovna.xml;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.PoslovniPartner;

@XmlRootElement(name = "poslovniPartner")
public class PoslovniPartnerXML {
	public Integer idPoslovnogPartnera;
	public String naziv;
	public String adresa;
	public String pib;
	public String vrsta;
	public String nazivPreduzeca;
	
	public PoslovniPartnerXML() {}
	
	public PoslovniPartnerXML(PoslovniPartner param) {
		idPoslovnogPartnera = param.getIdPoslovnogPartnera();
		naziv = param.getNaziv();
		adresa = param.getAdresa();
		pib = param.getPib();
		vrsta = param.getVrsta();
		nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
	
}
