package projekat.poslovna.xml;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.StavkeFakture;

@XmlRootElement(name = "stavkaFakture")
public class StavkeFaktureXML {
	public Integer idStavke;
	public float kolicina;
	public float rabat;
	public float jedinicnaCena;
	public float stopaPdvA;
	public float osnovica;
	public float iznosPdvA;
	public float ukupanIznos;
	public int brojFakture;
	public String nazivProizvoda;
	
	public StavkeFaktureXML() {}
	
	public StavkeFaktureXML(StavkeFakture param) {
		idStavke = param.getIdStavke();
		kolicina = param.getKolicina();
		rabat = param.getRabat();
		jedinicnaCena = param.getJedinicnaCena();
		stopaPdvA = param.getStopaPdvA();
		osnovica = param.getOsnovica();
		iznosPdvA = param.getIznosPdvA();
		ukupanIznos = param.getUkupanIznos();
		brojFakture = param.getIdFakture().getBrojFakture();
		nazivProizvoda = param.getIdProizvoda().getNazivProizvoda();
	}
	
}
