package projekat.poslovna.xml;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.PoslovnaGodina;

@XmlRootElement(name = "poslovnaGodina")
public class PoslovnaGodinaXML {
	public Integer idGodine;
	public int godina;
	public Short daLiJeZakljucena;
	public String nazivPreduzeca;
	
	public PoslovnaGodinaXML() {}
	
	public PoslovnaGodinaXML(PoslovnaGodina param) {
		idGodine = param.getIdGodine();
		godina = param.getGodina();
		daLiJeZakljucena = param.getDaLiJeZakljucena();
		nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
	
}
