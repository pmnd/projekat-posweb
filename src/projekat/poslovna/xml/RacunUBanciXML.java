package projekat.poslovna.xml;

import javax.xml.bind.annotation.XmlRootElement;

import projekat.poslovna.entity.RacunUBanci;

@XmlRootElement(name = "racunUBanci")
public class RacunUBanciXML {
	public Integer idRacuna;
	public String brojRacuna;
	public String nazivBanke;
	public String nazivPreduzeca;
	
	public RacunUBanciXML() {}
	
	public RacunUBanciXML(RacunUBanci param) {
		idRacuna = param.getIdRacuna();
		brojRacuna = param.getBrojRacuna();
		nazivBanke = param.getNazivBanke();
		nazivPreduzeca = param.getIdPreduzeca().getNazivPreduzeca();
	}
	
}
