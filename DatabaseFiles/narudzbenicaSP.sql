USE [poslovna]
GO
/****** Object:  StoredProcedure [dbo].[KreiranjeFaktureOdNarudzbine]    Script Date: 27-Jun-16 6:51:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- STORED PROCEDURA ZA GENERISANJE FAKTURE OD NARUDZBENICE!!!

ALTER PROCEDURE [dbo].[KreiranjeFaktureOdNarudzbine]	
	@IDNarudzbineParam int,	--ulazni parametar
	@OK int OUTPUT		--izlazni parametar
AS
	--telo procedure
	DECLARE
		@IDFakture int,
		@IDNarudzbine int,
		@IDgodine int,
		@datumFakture date

	SET @OK = 0 	--pesimisticki pristup

	PRINT 'param idNarudzbine ' + str(@IDNarudzbineParam)

	SELECT @IDNarudzbine = @IDNarudzbineParam	
	IF (SELECT COUNT(*) FROM narudzbenica WHERE ID_NARUDZBENICE = @IDNarudzbine) = 0
	BEGIN
		RAISERROR('Ne postoji narudzbenica sa trazenim id!',11,2)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM stavka_narudzbenice WHERE ID_NARUDZBENICE = @IDNarudzbine) = 0
	BEGIN
		RAISERROR('Ne postoje stavke za narudzbenicu sa trazenim id!',11,2)
		RETURN
	END
	
	SELECT @IDgodine = ID_GODINE FROM narudzbenica WHERE ID_NARUDZBENICE = @IDNarudzbine
	
	SELECT @datumFakture = DATUM_ FROM narudzbenica WHERE ID_NARUDZBENICE = @IDNarudzbine

	DECLARE --promenljive potrebene za kursor!!!
		@idProizvoda int,
		@kolicinaStavkeNar numeric(15,2),
		@jedCenaStavkeNar numeric(15,2),
		@stopaPDV numeric(15,2),
		@ukupnoPorezaZaProizvod int,
		@osnovica numeric(15,2),
		@iznosPDV numeric(15,2),
		@ukupanIznos numeric(15,2)
		
	DECLARE @stopaDatumTbl TABLE (stopa real, datum date)	

	BEGIN TRANSACTION

		DECLARE @OutputTbl TABLE (ID INT)
		DECLARE @IDPoslovnogPartneraNar int

		SELECT @IDPoslovnogPartneraNar = ID_POSLOVNOG_PARTNERA FROM narudzbenica;

		INSERT INTO faktura
			(ID_POSLOVNOG_PARTNERA, ID_GODINE, ID_NARUDZBENICE, BROJ_FAKTURE, DATUM_FAKTURE, DATUM_PLACANJA_FAKTURE,
				UKUPAN_RABAT, UKUPAN_IZNOS_BEZ_PDV_A, UKUPAN_PDV, UKUPNO_ZA_PLACANJE, STATUS)
			OUTPUT INSERTED.ID_FAKTURE INTO @OutputTbl(ID)
			VALUES (@IDPoslovnogPartneraNar, @IDgodine, @IDNarudzbine, 1010, @datumFakture, '11-11-2014', 0, 0, 0, 0, 1)
			--BITNA NAPOMENA: ID_FAKTURE TREBA DA BUDE AUTOINCREMENT
			--ID_POSLOVNOG_PARTNERA, BROJ_FAKTURE, DATUM_PLACANJA_FAKTURE se korisniku ostavlja za naknadnu izmenu!!!
			--U BAZI BI ZBOG OVOGA TREBALO UVEK IMATI POSLOVNOG PARTNERA SA ID = 1!!!!

		IF @@ERROR <> 0                      
		BEGIN
			PRINT 'Greska kod upisa!'
			ROLLBACK TRANSACTION
			RAISERROR('Kreiranje nove fakture nije uspelo!',11,2)
			RETURN
		END
		SELECT @IDFakture = ID FROM @OutputTbl
		
		PRINT 'novi idFakture ' + str(@IDFakture)
		
		
		
		-- Deklaracija kursora (SKUP TORKI STAVKI NARUDZBENICE SA -->ODGOVARAJUCIM<-- ID NARUDZBENICE

		DECLARE cursStavkeNardzbenice CURSOR 
		FOR SELECT ID_PROIZVODA, KOLICINA_STAVKE_NAR, JEDINICNA_CENA_STAVKE_NAR FROM stavka_narudzbenice WHERE ID_NARUDZBENICE = @IDNarudzbine
		-- Otvaranje kursora. Posle otvaranja, pokazivac na tekuci slog je ISPRED 
		-- PRVOG sloga: 
		OPEN cursStavkeNardzbenice
		-- Pristup prvom slogu u okviru kursora. 
		-- Sadrzaj sloga se kopira u navedene lokalne promenljive: 
		FETCH NEXT FROM cursStavkeNardzbenice INTO @idProizvoda, @kolicinaStavkeNar, @jedCenaStavkeNar	
		-- Kada je @@FETCH_STATUS = 0, to znaci da postoji tekuci slog (tj. jos 
		-- nismo stigli do kraja): 
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			--ZA SVAKI ODGOVARAJUCU STAVKU NARUDZBENICE KREIRATI ODGOVARAJUCU STAVKU FAKTURE
			INSERT INTO @stopaDatumTbl SELECT STOPA, DATUM_VAZENJA FROM stopa_poreza S, porez P, grupa_proizvoda G, proizvod PR
			WHERE PR.ID_PROIZVODA = @idProizvoda AND G.ID_GRUPE = PR.ID_GRUPE AND G.ID_POREZA = S.ID_POREZA AND 
				DATUM_VAZENJA < GETDATE() GROUP BY STOPA, DATUM_VAZENJA
				
				
			SELECT @ukupnoPorezaZaProizvod = COUNT(*) FROM @stopaDatumTbl
			PRINT 'UKUPNO POREZA ZA proizvod: ' + STR(@ukupnoPorezaZaProizvod)
			IF (@ukupnoPorezaZaProizvod = 0)
			BEGIN
				PRINT 'Greska kod upita za aktivan porez proizvoda!'
				ROLLBACK TRANSACTION
				RAISERROR('Ne postoji aktivan porez za zeljeni proizvod!',11,2)
				RETURN
			END
			
			SELECT @stopaPDV = STOPA FROM @stopaDatumTbl GROUP BY STOPA, datum HAVING datum = MAX(datum)
			--TRUNCATE TABLE @stopaDatumTbl
			DELETE FROM @stopaDatumTbl
			PRINT 'STOPA ZA proizvod: ' + STR(@stopaPDV)
			
			
			--racunanje potrebnih stvari za kompletiranje stavke fakture
			SET @osnovica = @kolicinaStavkeNar * @jedCenaStavkeNar
			SET @iznosPDV = @osnovica * @stopaPDV / 100
			SET @ukupanIznos = @osnovica + @iznosPDV	--RABAT NIJE URACUNATA JER JE PO DEFAULTU 0!!!
			
			INSERT INTO stavke_fakture
				(ID_PROIZVODA, ID_FAKTURE, KOLICINA, RABAT, JEDINICNA_CENA, STOPA_PDV_A, OSNOVICA, IZNOS_PDV_A, UKUPAN_IZNOS)
				VALUES (@idProizvoda, @IDFakture, @kolicinaStavkeNar, 0, @jedCenaStavkeNar, @stopaPDV, @osnovica, @iznosPDV, @ukupanIznos)	
			--BITNA NAPOMENA: ID STAVKE FAKTURE JE AUTOINCREMENT, NE SMEM GA SETOVATI!!!
			
			IF @@ERROR <> 0                      
			BEGIN
				PRINT 'Greska kod upisa!'
				ROLLBACK TRANSACTION
				RAISERROR('Kreiranje nove STAVKE fakture nije uspelo!',11,2)
				RETURN
			END
			PRINT 'UNETA STAVKA FAKTURE!'
			
			FETCH NEXT FROM cursStavkeNardzbenice INTO @idProizvoda, @kolicinaStavkeNar, @jedCenaStavkeNar
		END

		-- Kada kursor vise nije potreban, obavezno je zatvoriti ga i dealocirati 
		-- resurse koje zauzima!!!
		CLOSE cursStavkeNardzbenice
		DEALLOCATE cursStavkeNardzbenice
		
		
		
		DECLARE --promenljive potrebne za kursor!!!
			@idProizvodaStavkeF int,
			@iznosPDVaStavkeF DECIMAL(5,2),
			@idPorezaStavkeF int
		
		DECLARE @porezStavkiFtbl table (porezID int, iznosPDVa numeric(15,2))
			
		
		-- Deklaracija kursora (SKUP TORKI STAVKI FAKTURE SA -->ODGOVARAJUCIM<-- ID FAKTURE

		DECLARE cursStavkeFakture CURSOR 
		FOR SELECT ID_PROIZVODA, IZNOS_PDV_A FROM stavke_fakture WHERE ID_FAKTURE = @IDFakture
		-- Otvaranje kursora. Posle otvaranja, pokazivac na tekuci slog je ISPRED 
		-- PRVOG sloga: 
		OPEN cursStavkeFakture
		-- Pristup prvom slogu u okviru kursora. 
		-- Sadrzaj sloga se kopira u navedene lokalne promenljive: 
		FETCH NEXT FROM cursStavkeFakture INTO @idProizvodaStavkeF, @iznosPDVaStavkeF
		-- Kada je @@FETCH_STATUS = 0, to znaci da postoji tekuci slog (tj. jos 
		-- nismo stigli do kraja): 
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SELECT @idPorezaStavkeF = PO.ID_POREZA FROM proizvod PR, grupa_proizvoda G, porez PO
				WHERE PR.ID_PROIZVODA = @idProizvodaStavkeF AND PR.ID_GRUPE = G.ID_GRUPE AND G.ID_POREZA = PO.ID_POREZA
				
			INSERT INTO @porezStavkiFtbl (porezID, iznosPDVa) VALUES (@idPorezaStavkeF, @iznosPDVaStavkeF)
			IF @@ERROR <> 0                      
			BEGIN
				PRINT 'Greska kod upisa U PRIVREMENU TABELU!'
				ROLLBACK TRANSACTION
				RAISERROR('Kreiranje nove torke u privremenoj tabeli @porezStavkiFtbl nije uspelo!',11,2)
				RETURN
			END
			PRINT 'UNET porez u privremenu tabelu!'
			PRINT STR(@idPorezaStavkeF) + ' ' + STR(@iznosPDVaStavkeF)
			--UPISIVATI DIREKT U TABELU OBRACUNATI_POREZI => RADITI PROVERU DA LI POSTOJI TORKA SA ID_POREZA = @idPorezaStavkeF
					--AKO NE POSTOJI, DODATI NOVUL AKO POSTOJI SUMIRATI PDV NA POSTOJECOJ TORCI
			IF (SELECT COUNT(*) FROM obracunati_porezi WHERE ID_POREZA = @idPorezaStavkeF AND ID_FAKTURE = @IDFakture) = 0
			BEGIN
				PRINT 'TRENUTNO NE POSTOJI porez U OBRACUNATIM POREZIMA ZA ZELJENU FAKTURU SA ID_POREZA: ' + STR(@idPorezaStavkeF)
				INSERT INTO obracunati_porezi(ID_POREZA, ID_FAKTURE, IZNOS)
					VALUES (@idPorezaStavkeF, @IDFakture, CONVERT(REAL, @iznosPDVaStavkeF))
			END
			ELSE 
			BEGIN
				PRINT 'TRENUTNO NE POSTOJI porez U OBRACUNATIM POREZIMA ZA ZELJENU FAKTURU SA ID_POREZA: ' + STR(@idPorezaStavkeF)
				--UPDATE Customers
				--	SET ContactName='Alfred Schmidt', City='Hamburg'
				--	WHERE CustomerName='Alfreds Futterkiste'
				UPDATE obracunati_porezi
					SET IZNOS = CONVERT(REAL, IZNOS + @iznosPDVaStavkeF)
					WHERE ID_POREZA = @idPorezaStavkeF AND ID_FAKTURE = @IDFakture
			END
			
			FETCH NEXT FROM cursStavkeFakture INTO @idProizvodaStavkeF, @iznosPDVaStavkeF
		END

		-- Kada kursor vise nije potreban, obavezno je zatvoriti ga i dealocirati 
		-- resurse koje zauzima!!!
		CLOSE cursStavkeFakture
		DEALLOCATE cursStavkeFakture
		
		--AZURIRANJE TORKE FAKTURE SA ODGOVARAJUCIM ID-EM!
		--RACUNANJE VREDNOSTI POTREBNIH ZA KASNIJI UPIS U TU FAKTURU
			--UKUPAN_RABAT, UKUPAN_IZNOS_BEZ_PDV_A, UKUPAN_PDV, UKUPNO_ZA_PLACANJE
		DECLARE 
			@ukupanRabat float,
			@ukupanIznosBezPDV float,
			@ukupanPDV float,
			@ukupnoZaPlacanje float
			
		SELECT @ukupanRabat = SUM(OSNOVICA*RABAT/100), @ukupanIznosBezPDV = SUM(OSNOVICA), @ukupanPDV = SUM(IZNOS_PDV_A), @ukupnoZaPlacanje = SUM(OSNOVICA) - SUM(OSNOVICA*RABAT/100) + SUM(IZNOS_PDV_A)
			FROM stavke_fakture
			WHERE ID_FAKTURE = @IDFakture
		
		UPDATE faktura
					SET UKUPAN_RABAT = @ukupanRabat, UKUPAN_IZNOS_BEZ_PDV_A = @ukupanIznosBezPDV, UKUPAN_PDV = @ukupanPDV, UKUPNO_ZA_PLACANJE = @ukupnoZaPlacanje
					WHERE ID_FAKTURE = @IDFakture
		IF @@ERROR <> 0                      
			BEGIN
				PRINT 'NEUSPESNO AZURIRANJE TABELE'
				ROLLBACK TRANSACTION
				RAISERROR('Tabela faktura neuspesno azurirana',11,2)
				RETURN
			END
		
	
	COMMIT TRANSACTION


SET @OK = 1  --sve uredu

