USE [poslovna]
GO
/****** Object:  StoredProcedure [dbo].[KopiranjeCenovnika]    Script Date: 27-Jun-16 6:50:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- STORED PROCEDURA ZA KOPIRANJE CENOVNIKA!!!

ALTER PROCEDURE [dbo].[KopiranjeCenovnika]	
	@IDCenovnikaParam int,	--ulazni parametar
	@nazivParam varchar(255),	--ulazni parametar
	@vaziOdParam date,	--ulazni parametar (novi datum vazenja cenovnika)
	@procenatParam float, 	--ulazni parametar (faktor izmena cene, plusminus)
	@OK int OUTPUT		--izlazni parametar
AS
	--telo procedure
	DECLARE
		@IDCenovnika int,
		@naziv varchar(255),
		@vaziOd date, 
		@procenat float

	SET @OK = 0 	--pesimisticki pristup

	PRINT 'param idCenovnika ' + str(@IDCenovnikaParam)

	SELECT @IDCenovnika = @IDCenovnikaParam, @naziv = @nazivParam, @vaziOd = @vaziOdParam, @procenat = @procenatParam
	
	IF (SELECT COUNT(*) FROM cenovnik WHERE ID_CENOVNIKA = @IDCENOVNIKA) = 0
	BEGIN
		RAISERROR('Ne postoji cenovnik sa trazenim id!',11,2)
		RETURN
	END

	DECLARE --promenljive potrebene za kursos!!!
		@stariIDPreduzeca int,
		@stariIDCenovnika int,
		@noviIDCenovnika int,
		@stariIDStavkeCenovnika int,
		@stariIDProizvoda int,
		@staraCena float,
		@novaCena float

	SET @stariIDCenovnika = @IDCenovnikaParam
	SELECT @stariIDPreduzeca = ID_PREDUZECA FROM cenovnik 
		WHERE ID_CENOVNIKA = @IDCenovnika

	PRINT 'stari ID preduzeca ' + str(@stariIDPreduzeca)

	BEGIN TRANSACTION

		DECLARE @OutputTbl TABLE (ID INT)

		INSERT INTO cenovnik
  			(ID_PREDUZECA, VAZI_OD, NAZIV_CENOVNIKA)
  			OUTPUT INSERTED.ID_CENOVNIKA INTO @OutputTbl(ID)
			VALUES (@stariIDPreduzeca, @vaziOd, @naziv)
		--BITNA NAPOMENA!!! ID_CENOVNIKA BI TREBALO DA JE AUTOINCREMENT PA ZBOG TOGA TO NI TREBA INSERTOVATI!!!
			--ZBOG TOGA SAM STAVIO DA JE 1000!
			
		IF @@ERROR <> 0                      
		BEGIN
			PRINT 'Greska kod upisa!'
			ROLLBACK TRANSACTION
			RAISERROR('Kreiranje novog cenovnika nije uspelo!',11,2)
			RETURN
		END
		SELECT @noviIDCenovnika = ID FROM @OutputTbl
		PRINT 'novi idCenovnika ' + str(@noviIDCenovnika)
		
		
		-- Deklaracija kursora (SKUP TORKI STAVKI CENOVNIKA SA -->ODGOVARAJUCIM<-- ID CENOVNIKA

		DECLARE cursStavkeCenovnika CURSOR 
		FOR SELECT ID_PROIZVODA, CENA, ID_STAVKE_CENOVNIKA FROM stavke_cenovnika WHERE ID_CENOVNIKA = @stariIDCenovnika
		-- Otvaranje kursora. Posle otvaranja, pokazivac na tekuci slog je ISPRED 
		-- PRVOG sloga: 
		OPEN cursStavkeCenovnika
		-- Pristup prvom slogu u okviru kursora. 
		-- Sadrzaj sloga se kopira u navedene lokalne promenljive: 
		FETCH NEXT FROM cursStavkeCenovnika INTO @stariIDProizvoda, @staraCena, @stariIDStavkeCenovnika	--BITNO: UZIMATI I JEDINICNU KOLICINU PROIZVODA!!!!!!!!!!!!!!!!!!
		-- Kada je @@FETCH_STATUS = 0, to znaci da postoji tekuci slog (tj. jos 
		-- nismo stigli do kraja): 
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Obavljamo zeljenu akciju:   	
			PRINT LTRIM(STR(@stariIDProizvoda)) + '  ' + LTRIM(STR(@staraCena))
			
			--ZA SVAKI ODGOVARAJUCI PROZIVOD (STAVKU CENOVNIKA) URADITI KOPIRANJE U NOVI cenovnik I IZMENU CENE
			SET @novaCena = @staraCena + (@staraCena*@procenat/100)
			INSERT INTO stavke_cenovnika
  			(ID_CENOVNIKA, ID_PROIZVODA, CENA)
			VALUES (@noviIDCenovnika, @stariIDProizvoda, @novaCena)
			IF @@ERROR <> 0                      
			BEGIN
				PRINT 'Greska kod upisa NOVIH STAVKI CENOVNIKA!'
				ROLLBACK TRANSACTION
				RAISERROR('Kreiranje novih stavki cenovnika nije uspelo!',11,2)
				RETURN
			END
			--KRAJ IZMENE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
		    -- Pristup sledecem slogu:
			FETCH NEXT FROM cursStavkeCenovnika INTO @stariIDProizvoda, @staraCena, @stariIDStavkeCenovnika
		END

		-- Kada kursor vise nije potreban, obavezno je zatvoriti ga i dealocirati 
		-- resurse koje zauzima!!!
		CLOSE cursStavkeCenovnika
		DEALLOCATE cursStavkeCenovnika
		
	
	COMMIT TRANSACTION


SET @OK = 1  --sve uredu

