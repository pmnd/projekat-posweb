USE [poslovna]
GO

INSERT INTO preduzece VALUES ('Strazilovska 14,Novi Sad','Woo market','100006627');

INSERT INTO poslovna_godina VALUES (1, 2015, 1);
INSERT INTO poslovna_godina VALUES (0, 2016, 1);

INSERT INTO racun_u_banci VALUES ('105-0000000000999-39','Unicredit bank',1);
INSERT INTO racun_u_banci VALUES ('111-0000000000944-35','Erste bank',1);

INSERT INTO poslovni_partner VALUES ('Vuka Karadzica 44, Sirig','Max Direct','100002855','D',1);
INSERT INTO poslovni_partner VALUES ('Jevrejska 1, Novi Sad','Kabel','100002845','D',1);
INSERT INTO poslovni_partner VALUES ('Ljutice Bogdana 3, Beograd','Pex','100002833','KD',1);
INSERT INTO poslovni_partner VALUES ('Dositeja Obradovica 12, Becej','Alpe','100002115','D',1);

INSERT INTO cenovnik VALUES ('Cenovnik 1','12/04/2015',1);

INSERT INTO porez VALUES ('Porez na doprinos');
INSERT INTO porez VALUES ('Porez na dobit');

INSERT INTO stopa_poreza VALUES ('12/04/2015', 5.7, 1);
INSERT INTO stopa_poreza VALUES ('12/04/2015', 15.0, 2);

INSERT INTO grupa_proizvoda VALUES ('Pice',1,1);
INSERT INTO grupa_proizvoda VALUES ('Hrana',1,2);

INSERT INTO jedinica_mere VALUES ('Kilogram', 'kg');
INSERT INTO jedinica_mere VALUES ('Gram', 'g');
INSERT INTO jedinica_mere VALUES ('Litra', 'l');

INSERT INTO proizvod VALUES (0.5, 'Coca cola', 1, 3);
INSERT INTO proizvod VALUES (0.5, 'Sprite', 1, 3);
INSERT INTO proizvod VALUES (0.75, 'Jelen pivo', 1, 3);
INSERT INTO proizvod VALUES (1, 'Hleb', 2, 1);
INSERT INTO proizvod VALUES (0.5, 'Rostilj kobasica', 2, 1);

INSERT INTO stavke_cenovnika VALUES (80, 1, 1);
INSERT INTO stavke_cenovnika VALUES (80, 1, 2);
INSERT INTO stavke_cenovnika VALUES (120, 1, 3);
INSERT INTO stavke_cenovnika VALUES (40, 1, 4);
INSERT INTO stavke_cenovnika VALUES (250, 1, 5);

INSERT INTO narudzbenica VALUES (1,'11/04/2015',0,1,1);

INSERT INTO stavka_narudzbenice VALUES (250,1,1,5);
INSERT INTO stavka_narudzbenice VALUES (120,1,1,3);

INSERT INTO faktura VALUES (1,'12/04/2015','12/04/2015',1,370,50,20,370,1,2,1,1);

INSERT INTO stavke_fakture VALUES (5.7, 250, 1, 7, 3, 5.7, 250,1,5);
INSERT INTO stavke_fakture VALUES (5.7, 120, 1, 4, 2, 5.7, 120,1,3);

INSERT INTO obracunati_porezi VALUES (50, 1, 1);
INSERT INTO obracunati_porezi VALUES (20, 1, 2);

GO


