(function (angular){
    "use strict";

    angular.module('app.loginController', ['Alertify'])
        .run(function (Alertify) {
            
        })
        .controller('loginController', function ($cookies, $scope, $location, authentificationService, Session, Alertify) {

        
        
        $scope.login = function (user) {
                if(user.username == 'undefined' || user.password == 'undefined'){
                    return;
                }else{
                    authentificationService.login(user).success(function (res) {
                        var obj = {"token" : res.login.token, "email" : user.username};
                        Session.setUser(obj);
                        $scope.errorMesage = "";
                        $location.path("/home");
                        Alertify.success('Dobro dosli. Uspesno ste se ulogovali.');
                    }).error(function(err){
                         Alertify.error('Pogresno unet email ili lozinka.');
                    });
                   
                }
            }
        
        });
    
}(angular));