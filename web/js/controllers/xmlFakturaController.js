(function (angular){
    "use strict";

    angular.module('app.xmlFakturaController', ['Alertify'])
        .run(function (Alertify) {
            
        })
        .controller('xmlFakturaController', function ($scope, $routeParams,genericRequestService, Alertify) {
            var idFakture;
            $scope.xmlData = {};
            if ($routeParams.idFakture != undefined) {
                idFakture = $routeParams.idFakture;
                genericRequestService.getXmlFakture(idFakture).success(function(data){
                    $scope.xmlData = data;
                    Alertify.success('Uspesno ste generisali izvestaj.'); 
                }).error(function(data){
                    Alertify.error('Greska prilikom generisanja izvestaja.'); 
                });
            }
        
        
        });
    
}(angular));