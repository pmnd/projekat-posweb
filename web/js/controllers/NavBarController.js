(function (angular){
    "use strict";

    angular.module('app.navBarController', [])
        .controller('navBarController', function ($scope, $rootScope, semaService, $location, Session, authentificationService) {
        
            $scope.tables = [];
            $rootScope.email = Session.getUser().email;
        
            semaService.getSema().success(function(data){
                console.log(data);
                $scope.tables = semaService.getTables(data.root);
               
            });
        
            $scope.kliknuto = function(t){

            }
            
            $scope.clickOnTable = function(tableName){
                $location.path("/table/"+tableName);   
            }
            
            $scope.logout = function(){
                if(Session.isLoggedIn()){
                    authentificationService.logout().success(function (data) {
                        Session.destroyUser();
                        $location.path("/login");
                    });
                }
            }
  
        });
    
}(angular));