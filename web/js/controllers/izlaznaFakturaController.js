(function (angular){
    "use strict";

    angular.module('app.izlaznaFakturaController', [])
        .controller('izlaznaFakturaController', function ($scope, $rootScope, $location, $window) {

        $scope.showReport = function(data){
            var dateFrom = data.od.yyyymmdd();
            var dateTo = data.do.yyyymmdd();
          
            var url = "http://localhost:8081/jasperserver/flow.html?" +"_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports&reportUnit=%2Freports"
            +"%2FKif&standAlone=true"
            +"&dateBegin=" + dateFrom
            +"&dateEnd="+dateTo
            +"&output=pdf"
            console.log(url);
            
            $window.location.href = url;
            
        }
        });
    
}(angular));