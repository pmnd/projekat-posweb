(function (angular){
    "use strict";
    angular.module('app.genericTableController', ['Alertify'])
        .run(function (Alertify) {
            //Alertify.success('Dobro dosli!');
        })
        .controller('genericTableController', function ($scope, $rootScope, semaService, genericRequestService, $routeParams, $location, utilsService, validationService, Alertify, StackService, $window) {
            //***************** za next mehanizam
            var nextTableName; //zapravo NAZIV PARENT TABELE!!!
            var nextIdValue = -1;
            var isNextModule = false;
            $scope.parentIdName;
        
            //***************** za zoom mehanizam
            var zoomTableName;
            var zoomIdValue = -1;
            $scope.isZoomActive = false;
            var zoomCurrentState;
            
            if ($routeParams.nextTableName != undefined && $routeParams.idValue != undefined) {
                nextTableName = $routeParams.nextTableName;
                nextIdValue = $routeParams.idValue;
                isNextModule = true;
                $scope.parentIdName = semaService.getPrimaryKeyForTable(nextTableName);
            }
        
            if ($routeParams.zoomTableName != undefined && $routeParams.idValue != undefined) {
                zoomCurrentState = StackService.pop();
            }
        
            if ($location.path().indexOf("zoom") <= -1) {
                StackService.clearStack();
            }
        
            if (StackService.getSize() > 0) {
                $scope.isZoomActive = true;
            }

            //*****************************************
            var tableName= $routeParams.tableName;
            $scope.tableName = tableName;
            
            if (zoomCurrentState == undefined) {
                $scope.attributes = [];
                $scope.data = [];
                $scope.selectedItem = {};
                $scope.itemsNumber = -1;
                $scope.indexOfSelected = -1;
                $scope.IDofSelectedItem = -1;
                $scope.stateText = "PREGLED/IZMENA";
                $scope.tempFormData = {};
                $scope.filterData = {};
                //states : PREGLED/IZMENA 0    ADD 1     SEARCH 2
                $scope.tempState = 0;
                
            } else {
                //zoom mehanizam - restauracija
                $scope.attributes = [];
                $scope.data = zoomCurrentState.data;
                $scope.selectedItem = zoomCurrentState.selectedItem;
                $scope.itemsNumber = zoomCurrentState.itemsNumber;
                $scope.indexOfSelected = zoomCurrentState.indexOfSelected;
                $scope.IDofSelectedItem = zoomCurrentState.IDofSelectedItem;
                $scope.stateText = zoomCurrentState.stateText;
                $scope.tempFormData = zoomCurrentState.tempFormData;
                $scope.tempFormData[zoomCurrentState.refColumn] = parseInt($routeParams.idValue); //PREUZIMANJE ZOOM TORKE U COMBOBOX
                $scope.filterData = zoomCurrentState.filterData;
                //states : PREGLED/IZMENA 0    ADD 1     SEARCH 2
                $scope.tempState = zoomCurrentState.tempState;
            }
        
            
            
            //**********disableovanje odgovarajuceg comboboxa ako je next modul!
            if (isNextModule) {
                var x = $scope.parentIdName;
                $scope.tempFormData = {};
                $scope.tempFormData[x] = parseInt(nextIdValue);
            }
            //******************************************************************
        
            semaService.getSema().success(function(data){
                $scope.attributes = semaService.getAttributes(tableName, data.root);
                $scope.tableNameTOShow = semaService.getNameToShowForTable(tableName);
                $scope.childTables = semaService.getChildTables(tableName);
                $scope.specialActions = semaService.getSpecialActions(tableName);   
                $scope.specialActionsVisibility = [];
                for (var x in $scope.specialActions) {
                    $scope.specialActionsVisibility.push(false);
                }
                
                
                $rootScope.tableNameForReport = tableName;
                    
                var firstColFields = [];
                var secondColFields = [];
                var numOfVisibleAttributes = 0;
                $scope.lookupTables = [];
                var lookupValues = [];
                var lookupNames = [];
                var asyncIndexes = [];
                
                for (var i = 0; i < $scope.attributes.length; i++) {

                    if($scope.attributes[i].visibility == true){
                        numOfVisibleAttributes++;
                        
                        if($scope.attributes[i].isLookup){
                            asyncIndexes.push(i);
                            genericRequestService.getAll($scope.attributes[i].lookupTable).success(function(retData){
                                
                                Object.keys(retData).forEach(function(key,index) {
                                    // key: the name of the object key
                                    // index: the ordinal position of the key within the object
                                    for(var j = 0; j < asyncIndexes.length; j++){
                                        if(key == $scope.attributes[asyncIndexes[j]].lookupTable){
                                            $scope.attributes[asyncIndexes[j]].lookupValues = retData[key];
                                            asyncIndexes.splice(j, 1);
                                            break;
                                        }
                                    }
                                    
                                });
                                
                            });             
                        }
                    }
                }
                

                var addToFirstCol = true;
                for (var i = 0; i < $scope.attributes.length; i++) {
                    
                    if($scope.attributes[i].visibility == true){
                        if(addToFirstCol){
                             firstColFields.push($scope.attributes[i]);
                        }
                        else{
                            secondColFields.push($scope.attributes[i]);
                        }
                        addToFirstCol = !addToFirstCol;
                    }
                }
                
                $scope.firstColFields = firstColFields;
                $scope.secondColFields = secondColFields;
            });
            
            
            //funkcija koju treba pozivati kada zelimo da dobavimo sve torke
            $scope.getAll = function(){
                //raditi proveru da li je next mehanizam ukljucen!
                if (isNextModule) {
                    genericRequestService.getChildrenTupples(nextTableName, nextIdValue,tableName).success(function(retData){
                        $scope.data = utilsService.convertStringToDateInArrayObject($scope.attributes, retData[tableName]);
                        $scope.itemsNumber = $scope.data.length;
                    });
                } else {
                    genericRequestService.getAll(tableName).success(function(retData){
                        $scope.data = utilsService.convertStringToDateInArrayObject($scope.attributes, retData[tableName]);
                        $scope.itemsNumber = $scope.data.length;
                    });
                }
            }
            
            
            //pozivamo funkciju da dobavimo sve torke
            $scope.getAll();
        
            //Selektovanje redova tabele
            $scope.selectItem = function(item, index){
                $scope.enableField = "";
                $scope.enableValue= "";
                
                var tempObject = semaService.getDisableInfo($scope.tableName);
                if(tempObject != null){
                    $scope.enableField = tempObject.enableField;
                    $scope.enableValue= tempObject.enableValue;                  
                }
                
                //item.vaziOd = new Date(item.vaziOd);
                if($scope.tempState != 0)
                    $scope.changeState(0);
                    
                
                $scope.selectedItem = item;
                $scope.indexOfSelected = index;
                $scope.tempFormData = utilsService.convertStringToDateInObject($scope.attributes, JSON.parse(JSON.stringify($scope.selectedItem)));
                var kljuc = semaService.getPrimaryKeyForTable(tableName);
                $scope.IDofSelectedItem = item[kljuc];
                $rootScope.keyOfSelectedItem = $scope.IDofSelectedItem;
            }
            
            $scope.selectNext = function(){
                if($scope.itemsNumber > 0 && $scope.indexOfSelected != -1 ){
                    var indexOfSelected  = ($scope.indexOfSelected+1)%$scope.itemsNumber;
                    var selectedItem = $scope.data[indexOfSelected];
                    $scope.selectItem(selectedItem, indexOfSelected);
                }
            }
            
            $scope.selectPrevouse = function(){
                if($scope.itemsNumber > 0 && $scope.indexOfSelected != -1 ){
                    var indexOfSelected  = ($scope.indexOfSelected-1 + $scope.itemsNumber)%$scope.itemsNumber;
                    var selectedItem = $scope.data[indexOfSelected];
                    $scope.selectItem(selectedItem, indexOfSelected);
                }
            }
            
            $scope.selectFirst = function(){
                if($scope.itemsNumber > 0 ){
                    var indexOfSelected  = 0;
                    var selectedItem = $scope.data[indexOfSelected];
                    $scope.selectItem(selectedItem, indexOfSelected);
                }
            }
            $scope.selectLast = function(){
                if($scope.itemsNumber > 0 ){
                    var indexOfSelected  = $scope.itemsNumber-1;
                    var selectedItem = $scope.data[indexOfSelected];
                    $scope.selectItem(selectedItem, indexOfSelected);
                }
            }
            
            ////kraj selektovanja
            
            //menjanje stanja
            $scope.changeState = function(nextState){
                if(nextState == 0){
                    //popuniti polja EDIT VIEW
                    $scope.tempState = 0;
                    $scope.stateText = "PREGLED/IZMENA";
                }else if(nextState == 1){
                    //isprazniti polja ADD
                    $scope.tempState = 1;
                    $scope.stateText = "DODAVANJE";
                }else{
                    //isprazniti polja SEARCH
                    $scope.tempState = 2;
                    $scope.stateText = "PRETRAGA";
                    
                }
                $scope.initializeState();
            }
            
            $scope.initializeState = function(){
                if($scope.tempState == 0){
                    //$scope.filterData = {};
                }
                
            }
            
            $scope.add = function(){
                $scope.changeState(1);
                $scope.deselectRow();
                
            }
            
            $scope.specialAction = function(sa, indexOfSA){
                //ako ima vise specialaction-a, doci do odgovarajuceg po prepoznavanju restPath-a
                //ako pronadjeni specialaction ima polja za dijalog, otvoriti taj dijalog pa nakon njegovog popunjavanja, obaviti rest metodu
                $scope.dtoObjectSA = {};
                
                if(sa.restPath.indexOf('http') > -1){
                    $window.location.href = sa.restPath + '&fakturaID=' + $scope.IDofSelectedItem;
                    return;
                }
                
                if (sa.restPath.indexOf('exportXML') > -1) {
                    $location.path("/"+tableName+"/xml/" + $scope.IDofSelectedItem);
                    return;
                }
                
                if (sa.param == undefined) {
                    genericRequestService.specialAction(tableName+'/'+sa.restPath, $scope.IDofSelectedItem).success(function(data){
                    $scope.refresh();
                    Alertify.success('Uspesno pokrenuta akcija: ' + sa.show); 
                    $scope.specialActionsVisibility[saIndex] = false;     
                }).error(function(data){
                    Alertify.error('Greska prilikom pokretanja akcije: ' + sa.show);
                });
                } else {
                    $scope.specialActionsVisibility[indexOfSA] = true;
                }
            }
          
            
            $scope.deselectRow = function(){
                $scope.selectedItem = {};
                $scope.indexOfSelected = -1;
                $scope.IDofSelectedItem = -1;
                $rootScope.keyOfSelectedItem = $scope.IDofSelectedItem;
                $scope.tempFormData = {};
                $scope.filterData = {};
                if (isNextModule) {
                    var x = $scope.parentIdName;
                    $scope.tempFormData = {};
                    $scope.tempFormData[x] = parseInt(nextIdValue);
                }
            }
        
            $scope.delete = function(){
                if(!(Object.keys($scope.selectedItem).length === 0 && $scope.selectedItem.constructor === Object)){
                    var indexOfSelected = $scope.indexOfSelected;
                    genericRequestService.remove(tableName,$scope.IDofSelectedItem).success(function(data){
                        $scope.getAll();
                        $scope.deselectRow();
                    });
                    
                }else{
                    alert("Niste selektovali ni jedan red.");
                }
            }
            
            
            
            
            $scope.nextForm = function(){
                
            }
            
            $scope.search = function(){
                $scope.changeState(2);
                $scope.tempFormData = {};
                $scope.selectedItem = {};
                $scope.IDofSelectedItem = -1;
                if (isNextModule) {
                    var x = $scope.parentIdName;
                    $scope.tempFormData = {};
                    $scope.tempFormData[x] = parseInt(nextIdValue);
                }
            }
            
            $scope.refresh = function(){
                $scope.getAll();
                $scope.deselectRow();
            }
            
            $scope.commit = function(){
                
                var message = validationService.validateObject($scope.tableName, $scope.tempFormData);
                if(message == undefined || message == 'undefined'){
                    message = '';
                 }
                
                if($scope.tempState == 0){
                    //update
                    if(message != ""){
                        Alertify.error('Izmena nije uspela. ' + message);
                        return;
                    }
                     genericRequestService.update(tableName, $scope.tempFormData).success(function(data){
                        $scope.getAll();
                        Alertify.success('Izmena podataka je uspesno izvrsena.');
                     }).error(function(data){
                         Alertify.error('Izmena nije uspela. ' + message);
                     });
                    
                }else if($scope.tempState == 1){
                    //dodavanje
                    if(message != ""){
                        Alertify.error('Unos nije uspeo. ' + message);
                        return;
                    }
                    
                    genericRequestService.create(tableName, $scope.tempFormData).success(function(data){
                            $scope.getAll();
                            $scope.deselectRow();
                            Alertify.success('Podaci su uspesno uneseni.');
                    }).error(function(data){
                            Alertify.error('Unos nije uspeo. ' + message);
                    });
                }
                else{
                    //pretraga
                    $scope.filterData = utilsService.convertStringToDateInObject($scope.attributes, JSON.parse(JSON.stringify($scope.tempFormData)));
                }
            }
            
            $scope.rollback = function(){
                 if($scope.tempState == 0){
                    //update
                    $scope.deselectRow();
                    
                    
                }else if($scope.tempState == 1){
                    //dodavanje
                    $scope.deselectRow();
                    $scope.changeState(0);
                    
                }else{
                    //pretraga
                    $scope.deselectRow();
                    $scope.changeState(0);
                    
                }
                $scope.changeState(0);
            }
               
                
            $scope.clickOnChildTable = function(childTableName,refColumnName){
                
                //$location.path("/table/"+childTableName);
                
                var primaryKeyName = semaService.getPrimaryKeyForTable($routeParams.tableName);
                //$rootScope.nextFormFieldValue = $scope.selectedItem[primaryKeyName]; //ovome se kasnije pristupa kao $scope.nextFormFieldValue
                //$rootScope.nextFormFieldName = primaryKeyName;
                
                var nextFormIdValue = $scope.selectedItem[primaryKeyName];
                var nextFormIdName = primaryKeyName;
                //$location.path("/table/"+childTableName+"/"+nextFormIdName+"/"+nextFormIdValue);
                $location.path("/table/"+childTableName+"/"+tableName+"/"+nextFormIdValue);
            }
            
            $scope.zoomAction = function(nextTable, refColumn){
                var zoomStateForPush = {};
                
                zoomStateForPush.data = $scope.data;
                zoomStateForPush.selectedItem = $scope.selectedItem;
                zoomStateForPush.itemsNumber = $scope.itemsNumber;
                zoomStateForPush.indexOfSelected = $scope.indexOfSelected;
                zoomStateForPush.IDofSelectedItem = $scope.IDofSelectedItem;
                zoomStateForPush.stateText = $scope.stateText;
                zoomStateForPush.tempFormData = $scope.tempFormData;
                zoomStateForPush.filterData = $scope.filterData;
                zoomStateForPush.tempState = $scope.tempState;
                zoomStateForPush.refColumn = refColumn;
                
                zoomStateForPush.returnTableName = $scope.tableName;
                
                StackService.push(zoomStateForPush);
                
                $location.path("/table/"+nextTable+"/zoomIn");
            }
            
            $scope.zoomPickup = function(){
                // tablename - citam sa stacka, zoomtablename - this.tablename
                // /table/:tableName/zoom/:zoomTableName/:idValue
                var tableToReturnName = StackService.getLastState().returnTableName;
                
                $location.path("/table/"+tableToReturnName+"/zoom/"+tableName+"/"+$scope.IDofSelectedItem);
            }
            
            $scope.submitSpecialAction = function(sa, saIndex){
                for (var par in sa.param) {
                    if (!(sa.param[par].show)) {
                        $scope.dtoObjectSA[sa.param[par].text] = $scope.selectedItem[sa.param[par].refColumn];
                    }
                }
                genericRequestService.specialActionWithParams(tableName+'/'+sa.restPath, $scope.dtoObjectSA, tableName).success(function(data){
                    $scope.refresh();
                    Alertify.success('Uspesno pokrenuta akcija: ' + sa.show); 
                    $scope.specialActionsVisibility[saIndex] = false;     
                }).error(function(data){
                    Alertify.error('Greska prilikom pokretanja akcije: ' + sa.show);
                });
            }
            
            $scope.cancelSpecialAction = function(saIndex){
                $scope.specialActionsVisibility[saIndex] = false;
            }
           
        });
    
}(angular));