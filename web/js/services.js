//insert new service in list

angular.module('app.services', [
            'app.semaService',
            'app.genericRequestService',
            'app.validationService',
            'app.utilsService',
            'app.Session',
            'app.authentificationService',
            'app.StackService'
]);