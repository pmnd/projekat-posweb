//insert new controller in list

angular.module('app.controllers', [
    'app.mainController',
    'app.navBarController',
    'app.genericTableController',
    'app.fakturaSaStavkamaController',
    'app.izlaznaFakturaController',
    'app.xmlFakturaController',
    'app.loginController'
]);