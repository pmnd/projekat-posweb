(function (angular) {
    "use strict";
    angular.module('app', ['smart-table','app.controllers','ngCookies','ngAnimate', 'app.services','app.directives', 'ngRoute'])
        .config(function ($routeProvider, $locationProvider, $httpProvider) {
            $routeProvider
                .when('/', {
                    redirectTo: '/login'
                })
                .when('/home', {
                    templateUrl: 'templates/main.html',
                    controller: 'mainController'
                }).when('/table/:tableName', {
                    templateUrl: 'templates/genericTable.html',
                    controller: 'genericTableController'
                }).when('/table/:tableName/zoom/:zoomTableName/:idValue', {
                    templateUrl: 'templates/genericTable.html',
                    controller: 'genericTableController'
                }).when('/table/:tableName/zoomIn', {
                    templateUrl: 'templates/genericTable.html',
                    controller: 'genericTableController'
                }).when('/table/:tableName/:nextTableName/:idValue', {
                    templateUrl: 'templates/genericTable.html',
                    controller: 'genericTableController'
                }).when('/login', {
                    templateUrl: 'templates/login.html',
                    controller: 'loginController'
                }).when('/izlaznaFaktura', {
                    templateUrl: 'templates/izlaznaFaktura.html',
                    controller: 'izlaznaFakturaController'
                }).when('/faktura/xml/:idFakture', {
                    templateUrl: 'templates/xmlFaktura.html',
                    controller: 'xmlFakturaController'
                }).when('/fakturaSaStavkama', {
                    templateUrl: 'templates/fakturaSaStavkama.html',
                    controller: 'fakturaSaStavkamaController'
                }).otherwise('/');

        }).run(['$rootScope', '$location', 'Session', function ($rootScope, $location, Session, semaService) {

        Date.prototype.yyyymmdd = function() {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
            var dd  = this.getDate().toString();
            return yyyy +''+ (mm[1]?mm:"0"+mm[0]) +''+ (dd[1]?dd:"0"+dd[0]); // padding
        };

        $rootScope.$on('$routeChangeStart', function (event, next) {
            
            if (!Session.isLoggedIn()) {
                $location.path('/login');
            }
            else {
                if($location.path() == '/login')
                     $location.path('/home');
            }
            });
        }]);
    
}(angular));
