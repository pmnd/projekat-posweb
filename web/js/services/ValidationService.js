(function (angular){
    "use strict";

    angular.module('app.validationService', [])
        .factory('validationService', function ($http, semaService) {
            var retVal = {};
            
            retVal.validateObject = function(tableName, validationObject){
                var attributes = [];
                var intRegex = /^\d+$/;
                var realRegex = /^\d+.?\d*$/;
                var message = "";
                
                    retVal.sema = semaService.getSemaAttr();
                    
                    for(var a in retVal.sema.table){
                        if(retVal.sema.table[a].name == tableName){
                            attributes = retVal.sema.table[a].field;
                            var type;
                            var primaryKey = retVal.sema.table[a].primary_key.value;

                            for(var f in attributes){
                                if(attributes[f].varName == primaryKey){
                                    continue;
                                }
                                if (validationObject.hasOwnProperty(attributes[f].varName)) {
                                    if(attributes[f].type == 'int'){
                                        if(intRegex.test(validationObject[attributes[f].varName]) == false){
                                            if(message == ""){
                                                message = "Nevalidno uneta polja: " + attributes[f].show;
                                            }
                                            else{
                                                message += ", " + attributes[f].show;
                                            }
                                        } 
                                    }
                                    else if(attributes[f].type == 'real'){
                                        if(realRegex.test(validationObject[attributes[f].varName]) == false){
                                            if(message == ""){
                                                message = "Nevalidno uneta polja: " + attributes[f].show;
                                            }
                                            else{
                                                message += ", " + attributes[f].show;
                                            }
                                        }
                                    }
                                }
                                else{
                                    if(message == ""){
                                        message = "Nevalidno uneta polja: " + attributes[f].show;
                                    }
                                    else{
                                         message += ", " + attributes[f].show;
                                    }
                                }
                                
                            }
                            return message;
                        } 
                    }
                    return message;
            }
  
            return retVal;
        });
    
    
    
}(angular));