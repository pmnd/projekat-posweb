(function (angular) {
    "use strict";
    angular.module('app.StackService', [])
        .factory('StackService', function(){
            
            var stack = [];
            var retVal = {};
        
            retVal.push = function(state){
                stack.push(state);
            }
            
            retVal.pop = function(){
                return stack.pop();
            }
            
            retVal.getSize = function(){
                return stack.length;
            }
            
            retVal.getLastState = function(){
                return stack[stack.length-1];
            }
            
            retVal.clearStack = function(){
                stack = [];
            }
        
            return retVal;
        
    });
}(angular));