(function (angular) {
    "use strict";
    angular.module('app.Session', [])
        .factory('Session', function($cookies){

            var logedIn = false;
        
            return{
                setUser : function(aUser){
                    $cookies.put("token", aUser.token);
                    $cookies.put("email", aUser.email);
                    logedIn = true;
                },
                getUser : function(){
                    return {'token': $cookies.get("token"), 'email': $cookies.get("email")};
                },
                
                destroyUser : function(){
                  $cookies.remove("token");
                  $cookies.remove("email");
                  logedIn = false;
                },
                
                isLoggedIn : function(){
                    if($cookies.get("token") != undefined && $cookies.get("email") != undefined)
                        return true;
                    else
                        return false;
                },
                logedIn : logedIn
              }
            });    
    
}(angular));