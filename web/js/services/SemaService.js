(function (angular){
    "use strict";

    angular.module('app.semaService', [])
        .factory('semaService', function ($http, Session) {
            var retVal = {};
            retVal.sema = {};
            retVal.tables = [];
        
            retVal.getSema = function(){
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/sema',
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
            }
    
            retVal.getSemaAttr = function(){
                return retVal.sema;
            }
            
            retVal.getTables = function(sema){
                if(sema != undefined)
                    retVal.sema = sema;
                
                var tables = [];
                for(var t in retVal.sema.table){
                    tables.push({'name':retVal.sema.table[t].name, 'show': retVal.sema.table[t].show, 'enableField': retVal.sema.table[t].enableField, 'enableValue': retVal.sema.table[t].enableValue});
                }
                retVal.tables = tables;
                return tables;
            }
            
            
            retVal.getNameToShowForTable = function(tableName){
                for(var t in retVal.tables){
                    if(retVal.tables[t].name == tableName)
                        return retVal.tables[t].show;
                }
            }
            
            retVal.getDisableInfo = function(tableName){
                for(var t in retVal.tables){
                    if(retVal.tables[t].name == tableName){
                        if(retVal.tables[t].enableField == undefined || retVal.tables[t].enableValue == undefined){
                            return null;
                        }
                        return {enableField : retVal.tables[t].enableField, enableValue: retVal.tables[t].enableValue};
                    }
                }
            }
            
            retVal.getAttributes = function(tableName, sema){
                retVal.sema = sema;
                var attributes = [];
                var ret = [];
                for(var a in retVal.sema.table){
                    if(retVal.sema.table[a].name == tableName){
                        attributes = retVal.sema.table[a].field;
                        var type;
                        
                        for(var f in attributes){
                            
                            var enabled = true;
                            if(attributes[f].type == 'date' || attributes[f].type == 'datetime' || attributes[f].type == 'datetime2' ){
                                type = "date";
                            }
                            else{
                                type = "text";
                            }
                            
                            if(attributes[f].hasOwnProperty('enabled')){
                                enabled = attributes[f].enabled;
                            }
                            
                            ret.push({'show':attributes[f].show, 'name': attributes[f].text, 'type': type, 'visibility' : JSON.parse(attributes[f].visibility), varName: attributes[f].varName, isLookup: false, enabled: enabled });
                        }
                        if(retVal.sema.table[a].parent_tables != undefined){
                            attributes = retVal.sema.table[a].parent_tables.value;
                            if(attributes instanceof Array)
                                for(var f in attributes){
                                    var enabled = true;
                                    
                                    if(attributes[f].type == 'date' || attributes[f].type == 'datetime' || attributes[f].type == 'datetime2' ){
                                        type = "date";
                                    }
                                    else{
                                        type = "text";
                                    }
                                    
                                    if(attributes[f].hasOwnProperty('enabled')){
                                        enabled = attributes[f].enabled;
                                    }
                                    
                                    ret.push({'show':attributes[f].show, 'name': attributes[f].lookupColumn, 'type': type, 'visibility' : true, varName: attributes[f].varName, isLookup: true, lookupTable: attributes[f].text, refColumn: attributes[f].refColumn, lookupValues: [], enabled: enabled });
                            }
                            else{
                                type = "text";
                                var enabled = true;
                                if(attributes.hasOwnProperty('enabled')){
                                    enabled = attributes.enabled;
                                }
                                
                                ret.push({'show':attributes.show, 'name': attributes.lookupColumn, 'type': type, 'visibility' : true, varName: attributes.varName, isLookup: true, lookupTable: attributes.text, refColumn: attributes.refColumn, lookupValues : [], enabled: enabled });
                            }
                        }
                        
                        return ret;
                    } 
                }
                return [];
            }
            
            retVal.getSpecialActions = function(tableName) {
                var ret = [];
                var values = [];
                for(var a in retVal.sema.table){
                    if(retVal.sema.table[a].name == tableName){
                        if (retVal.sema.table[a].special_action != undefined) {
                            values = retVal.sema.table[a].special_action.value;
                            if(values instanceof Array) {
                                for(var v in values){
                                    if(values[v].param != undefined)
                                        ret.push({'show':values[v].show, 'restPath':values[v].restPath, 'param' : values[v].param});
                                    else
                                        ret.push({'show':values[v].show, 'restPath':values[v].restPath});
                                }
                            } else {
                                if(values.param != undefined)
                                    ret.push({'show':values.show, 'restPath':values.restPath, 'param' : values.param});
                                else
                                    ret.push({'show':values.show, 'restPath':values.restPath});
                            }
                        } 
                    }
                }
                console.log(ret);
                return ret;
            }
            
            retVal.getPrimaryKeyForTable = function(tableName){
                var attributes = [];
                var ret = [];
                for(var a in retVal.sema.table){
                    if(retVal.sema.table[a].name == tableName){
                        return retVal.sema.table[a].primary_key.value;
                    }
                }
                
                return [];
            }
            
            retVal.getParentTables = function(tableName){
                
            }
            
            retVal.getChildTables = function(tableName){
                var attributes = [];
                var ret = [];
                for(var a in retVal.sema.table){
                    if(retVal.sema.table[a].name == tableName){
                        
                        if(retVal.sema.table[a].child_tables != undefined){
                            attributes = retVal.sema.table[a].child_tables.value;
                            if(attributes instanceof Array){
                                for(var f in attributes){
                                    ret.push({'name': attributes[f].text, 'show': attributes[f].show});
                                }
                            }
                            else{
                                ret.push({'name': attributes.text, 'show': attributes.show});
                            }
                        }
                        
                        return ret;
                        
                    }
                }
                return [];
            }
            
           
            return retVal;
        });
    
    
    
}(angular));