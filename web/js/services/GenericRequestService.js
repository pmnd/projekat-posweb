(function (angular){
    "use strict";

    angular.module('app.genericRequestService', [])
        .factory('genericRequestService', function ($http, Session) {
            var retVal = {};
        
            retVal.getAll = function(tableName){
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
            }
            
            retVal.getOne = function(tableName, id){
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName +'/'+id,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
            }
            
            retVal.create = function(tableName, object){
                var temp = {};
                temp[tableName] = object;
                
                var req = {
                method: 'POST',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName,
                headers: {
                    'Authorization': Session.getUser().token
                    },
                data: temp
                }

                return $http(req);
            }
            
            retVal.update = function(tableName, object){
                
                var temp = {};
                temp[tableName] = object;
                
                var req = {
                method: 'PUT',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName,
                headers: {
                    'Authorization': Session.getUser().token
                    },
                data: temp
                }

                return $http(req);
                
            }
            
            retVal.remove = function(tableName, id){
                
                var req = {
                method: 'DELETE',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName +'/'+id,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
               
            }
  
            retVal.getChildrenTupples = function(tableName, id, childTableName){
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/'+tableName+'/'+id+'/'+childTableName,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
            }
            
            retVal.specialAction = function(path, id){
                
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/'+path+id,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }

                return $http(req);
            }
            
            retVal.specialActionWithParams = function(path, dtoObject, tableName){
                var dtoData = {};
                dtoData[tableName] = dtoObject;
                var req = {
                method: 'POST',
                url: 'http://localhost:8080/POSLOVNA/rest/'+path,
                headers: {
                    'Authorization': Session.getUser().token
                    },
                    data : dtoData
                }
                    
                return $http(req);
            }
            
             retVal.getXmlFakture = function(idFakture){
                var req = {
                method: 'GET',
                url: 'http://localhost:8080/POSLOVNA/rest/faktura/exportXML/'+idFakture,
                headers: {
                    'Authorization': Session.getUser().token
                    }
                }
                    
                return $http(req);
            }
            
            return retVal;
        });
    
    
    
}(angular));