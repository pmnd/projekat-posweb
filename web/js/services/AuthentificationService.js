(function (angular) {
    "use strict";
    angular.module('app.authentificationService', [])
        .factory('authentificationService', function ($http, Session) {
        
        
        var authService = {};

        authService.login = function (credentials) {            
            return $http.post('http://localhost:8080/POSLOVNA/rest/korisnik/login', 
                              {korisnik:{ username: credentials.username, password: credentials.password}});
        }
        
    

        
        authService.logout = function(){
            var req = {
            method: 'POST',
            url: 'http://localhost:8080/POSLOVNA/rest/korisnik/logout',
            headers: {
            'Authorization': Session.getUser().token
            }
            }

            return $http(req);
        }
        
        return authService;
    });
    
    
}(angular));