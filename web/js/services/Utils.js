(function (angular){
    "use strict";

    angular.module('app.utilsService', [])
        .factory('utilsService', function () {
            var retVal = {};
            
            retVal.convertStringToDateInArrayObject = function(attributes, arrayOfObjects){
                for(var a in attributes){
                    if(attributes[a].type == 'date'){
                        var p = attributes[a].varName;
                        for(var i in arrayOfObjects){
                            var object = arrayOfObjects[i];
                            object[p] = new Date(object[p]);
                        }
                        
                    }
                    var p = attributes[a].varName;
                    for(var i in arrayOfObjects){
                        var object = arrayOfObjects[i];
                        if(object[p] == undefined)
                            object[p] = '';
                    }
                    
                }
                
                return arrayOfObjects;
            } 
            
            retVal.convertStringToDateInObject = function(attributes, object){
                for(var a in attributes){
                    if(attributes[a].type == 'date'){
                        var p = attributes[a].varName;
                        object[p] = new Date(object[p]);
                        
                        
                    }
                    var p = attributes[a].varName;
                    if(object[p] == undefined)
                            object[p] = '';
                }
                
                return object;
            } 
            
            return retVal;
        });
    
    
    
}(angular));